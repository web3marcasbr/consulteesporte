<?php

// Home
Breadcrumbs::register('home', function($breadcrumbs)
{
    $breadcrumbs->push('Home', url('admin'));
});

/**
 * Price breadcrumbs
 */

// Home > Ofertas
Breadcrumbs::register('price', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Ofertas', route('preco.index'));
});

// Home > Ofertas > Create
Breadcrumbs::register('price_create', function($breadcrumbs)
{
    $breadcrumbs->parent('price');
    $breadcrumbs->push('Novo', route('preco.create'));
});

// Home > Ofertas > Show
Breadcrumbs::register('price_show', function($breadcrumbs, $category)
{
    $breadcrumbs->parent('price');
    $breadcrumbs->push($category->name, route('preco.show', $category->id));
});

// Home > Blog > [Edit]
Breadcrumbs::register('price_edit', function($breadcrumbs, $category)
{
    $breadcrumbs->parent('price');
    $breadcrumbs->push($category->name, route('preco.edit', $category->id));
});

/**
 * Post breadcrumb
 */
// Home > Ofertas
Breadcrumbs::register('post', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Postagens', route('post.index'));
});

// Home > Ofertas > Create
Breadcrumbs::register('post_create', function($breadcrumbs)
{
    $breadcrumbs->parent('post');
    $breadcrumbs->push('Novo', route('post.create'));
});

// Home > Ofertas > Show
Breadcrumbs::register('post_show', function($breadcrumbs, $category)
{
    $breadcrumbs->parent('post');
    $breadcrumbs->push($category->title, route('post.show', $category->id));
});

// Home > Blog > [Edit]
Breadcrumbs::register('post_edit', function($breadcrumbs, $category)
{
    $breadcrumbs->parent('post');
    $breadcrumbs->push($category->title, route('post.edit', $category->id));
});

/**
 * Students breadcrumb
 */
Breadcrumbs::register('student', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Alunos', route('alunos.index'));
});

// Home > Ofertas > Create
Breadcrumbs::register('student_create', function($breadcrumbs)
{
    $breadcrumbs->parent('student');
    $breadcrumbs->push('Novo', route('alunos.create'));
});

// Home > Ofertas > Show
Breadcrumbs::register('student_show', function($breadcrumbs, $category)
{
    $breadcrumbs->parent('student');
    $breadcrumbs->push($category->name, route('alunos.show', $category->id));
});

// Home > Blog > [Edit]
Breadcrumbs::register('student_edit', function($breadcrumbs, $category)
{
    $breadcrumbs->parent('student');
    $breadcrumbs->push($category->name, route('alunos.edit', $category->id));
});

/**
 * Testimonial breadcrumb
 */
Breadcrumbs::register('testimonial', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Depoimentos', route('depoimentos.index'));
});

// Home > Ofertas > Create
Breadcrumbs::register('testimonial_create', function($breadcrumbs)
{
    $breadcrumbs->parent('testimonial');
    $breadcrumbs->push('Novo', route('depoimentos.create'));
});

// Home > Ofertas > Show
Breadcrumbs::register('testimonial_show', function($breadcrumbs, $category)
{
    $breadcrumbs->parent('testimonial');
    $breadcrumbs->push($category->name, route('depoimentos.show', $category->id));
});

// Home > Blog > [Edit]
Breadcrumbs::register('testimonial_edit', function($breadcrumbs, $category)
{
    $breadcrumbs->parent('testimonial');
    $breadcrumbs->push($category->name, route('depoimentos.edit', $category->id));
});

/**
 * Partner breadcrumb
 */
Breadcrumbs::register('partner', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Parceiros', route('parceria.index'));
});

// Home > Ofertas > Create
Breadcrumbs::register('partner_create', function($breadcrumbs)
{
    $breadcrumbs->parent('partner');
    $breadcrumbs->push('Novo', route('parceria.create'));
});

// Home > Ofertas > Show
Breadcrumbs::register('partner_show', function($breadcrumbs, $category)
{
    $breadcrumbs->parent('partner');
    $breadcrumbs->push($category->nome, route('parceria.show', $category->id));
});

// Home > Blog > [Edit]
Breadcrumbs::register('partner_edit', function($breadcrumbs, $category)
{
    $breadcrumbs->parent('partner');
    $breadcrumbs->push($category->nome, route('parceria.edit', $category->id));
});

