<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('master');
});

Route::get('/blog', 'BlogController@blogAll');

Route::get('/', 'IndexController@index');

Route::get('post/{id}', 'IndexController@showBlog');

Route::post('/depoimento/create', 'IndexController@TestimonialStore');

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('/send', 'EmailController@send');

Route::get('/blog/exclusivo/{id}', 'HomeController@blog');

Route::resource('comentario', 'UserCommentController');

Route::group(['prefix' => 'admin', 'middleware' => ['admin']], function () {

    Route::get('/', 'IndexController@Dashboardindex');
    Route::resource('comentarios', 'CommentController');
    Route::resource('image', 'ImageController');
    Route::resource('newsletter', 'NewsController');
    Route::resource('parceria', 'PartnerController');
    Route::resource('post', 'PostController');
    Route::resource('preco', 'PriceController');
    Route::resource('depoimentos', 'TestimonialController');
    Route::resource('alunos', 'StudentController');

    Route::get('downloadNewsletter', 'AdminController@downloadNewsletter');
});
