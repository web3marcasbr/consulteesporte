<!DOCTYPE html>
<html>
<head>

    @include('master.metatags')

    <title> @lang('client.name') @yield('title')</title>

    @include('master.css')

</head>

<body>

@include('page.navbar')

@include('page.home')

@include('page.socialmedia')

@include('page.team')

@include('page.pricing')

@include('page.blog')

@include('page.testimonial')

@include('page.partners')

@include('page.contact')

{{-- @include('page.services') --}}

@include('master.footer')

@include('master.js')

</body>

<!-- JS Plugins -->
</html>
