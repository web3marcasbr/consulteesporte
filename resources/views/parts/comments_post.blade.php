<div class="col-md-12">

    <div class="">

        <div class="well">
            <h4>Deixe um comentário:</h4>

            {!! BootForm::open()->action(route('comentario.store')) !!}

            {!! BootForm::hidden('post_id')->value($post->id) !!}
            {!! BootForm::text('Título do comentário', 'title')->placeholder('Escreva algo caso deseje dar uma descrição do seu comentário') !!}
            {!! BootForm::textarea('Escreva o comentário', 'comment')->required()->rows(3) !!}

            {!! BootForm::submit('Postar comentário')->addClass('btn btn-primary') !!}

            {!! BootForm::close() !!}

        </div>

        <h4> Comentários </h4>

        @include('page.comments')

    </div>
</div>

