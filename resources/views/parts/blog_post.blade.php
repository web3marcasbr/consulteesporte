<div class="col-md-4">
    <article class="">
        <div class="">
            <img class="blog-img" src="{{ asset( $post->image_url ) }}" alt="image">
        </div>
        <div class="col-md-12">
            <small>
                <div class="blog-post">
                    <h6> {{ $post->title }} </h6>
                    <p> {{ $post->subtitle }} </p>
                </div>
            </small>
            <p>
                Postado em:
                <i class="fa fa-clock-o"
                   aria-hidden="true"></i> {{ $post->created_at->format('d/m/y') }}
                as {{ $post->created_at->format('H:i') }}
            </p>

            @if(Request::is('home'))
                <a href="{{ url('blog/exclusivo/' . $post->id) }}" class="read_more"><p> Ler mais <i
                                class="fa fa-long-arrow-right" aria-hidden="true"></i></p></a>
            @else
                <a href="{{ url('post/' . $post->id) }}" class="read_more"><p> Ler mais <i
                                class="fa fa-long-arrow-right" aria-hidden="true"></i></p></a>
            @endif


        </div>
    </article>

    {{--
    <article class="our_blog">
        <div class="blog_image">
            <img src="{{ asset( $post->image_url ) }}" alt="image">
        </div>

        <div class="blog_detail">
            <div class="category_heading">
                <a href="#"> <h6> {{ $post->title }} </h6> </a>
                <a href="#"> <h5> {{ $post->subtitle }} </h5> </a>

                <ul>
                    <li> <i class="fa fa-clock-o" aria-hidden="true"></i> {{ $post->created_at }} </li>
                    <!--<li> <a href="#"> <i class="fa fa-comments-o" aria-hidden="true"></i> Comments </a> </li>-->
                </ul>

                <a href="{{ url('post/' . $post->id) }}" class="read_more"> <p> Ler mais <i class="fa fa-long-arrow-right" aria-hidden="true"></i> </p> </a>
            </div>
        </div>
    </article>
    --}}
</div>