<section id="portfolio" class="fifth-bg">
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <div class="section_heading">
                    <h2> @lang('partner.title') </h2>

                    {{--<h4> @lang('partner.subtitle') </h4> --}}
                </div>

                @foreach ($partners as $partner)
                    <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                        <div class="portfolio_img">
                            <a href="{{ $partner->link }}" class="thumbnail" target="_blank">
                                <img class="image xsmedium" src="{{ asset($partner->image_id) }}"
                                     alt="{{ $partner->nome }}">
                            </a>
                        </div>
                    </div>
                @endforeach

            </div>
        </div>
    </div>
</section>