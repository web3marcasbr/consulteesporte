<!--<section id="home" class="top_banner_bg secondary-bg">-->

<div class="container">
    <div class="row">
        <div class="col-md-12">

            <div class="section_heading" id='post_view' style="margin-top: 80px;">
                <img class="" src="{{ asset( $post->image_url ) }}" alt="image">
                <h2> {{ $post->title }} </h2>

                <h4>
                    <small> {{ $post->subtitle }} </small>
                </h4>
            </div>

            <!-- Member #1 -->
            <div class="col-md-10 col-md-offset-1">

                <article>

                    {!! $post->text !!}

                </article>

                <br>
                <p><span class="glyphicon glyphicon-time"></span>
                    Postado em
                    {{ $post->created_at->format('d/m/y') }}
                    as {{ $post->created_at->format('H:i') }}
                    por {{ $post->user->name or 'Anonimo'}}
                </p>

                @if (Request::is('blog/exclusivo/*'))
                    @include('parts.comments_post')
                @endif

                @if (Request::is('post/*'))
                    <a class="btn btn-small btn-default" href="{{ url('/blog') }}"> Ver outras postagens</a>
                @else
                    <a class="btn btn-small btn-default" href="{{ url('/home') }}"> Ver outras postagens</a>
                @endif

                <div class="divider"></div>

            </div>
        </div>
    </div>
</div>