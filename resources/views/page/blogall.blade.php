<!DOCTYPE html>
<html>
<head>

    @include('master.metatags')

    <title> @lang('client.name') @yield('title')</title>

    @include('master.css')

</head>

<body>

    <header class="navbar-fixed-top fixnav">
        <div class="container">
            <div class="row">
                <div class="header_top">
                    <div class="col-md-2">
                        <div class="logo_img">
                            <a href="{{ url('/') }}"><img src="{{ asset('custom_img/logo.png') }}" alt="logoimage"></a>
                        </div>
                    </div>

                    <div class="col-md-10">
                        <div class="menu_bar">
                            <nav role="navigation" class="navbar navbar-default">
                                <div class="navbar-header">
                                    <button id="menu_slide" aria-controls="navbar" aria-expanded="false"
                                    data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>

                            <div class="collapse navbar-collapse" id="navbar">

                                <ul class="nav navbar-nav">

                                    @section('home')
                                    <li><a href="{{ url('/#home') }}" class="js-target-scroll">@lang('home.navbar_name')</a></li>
                                    @show

                                    @section('team')
                                    <li><a href="{{ url('/#team') }}" class="js-target-scroll">@lang('team.navbar_name')</a></li>
                                    @show

                                    @section('pricing')
                                    <li><a href="{{ url('/#pricing') }}" class="js-target-scroll">@lang('price.navbar_name')</a>
                                    </li>
                                    @show

                                    @section('blog')
                                    <li><a href="{{ url('/#blog') }}" class="js-target-scroll">@lang('post.navbar_name')</a></li>
                                    @show

                                    @section('testimonial')
                                    <li><a href="{{ url('/#testimonial') }}" class="js-target-scroll">@lang('testimonial.navbar_name')</a></li>
                                    @show

                                    @section('portfolio')
                                    <li><a href="{{ url('/#portfolio') }}" class="js-target-scroll">@lang('partner.navbar_name')</a></li>
                                    @show

                                    @section('contact')
                                    <li><a href="{{ url('/#contact') }}" class="js-target-scroll">@lang('contact.navbar_name')</a>
                                    </li>
                                    @show

                                </ul>
                            </div>

                        </nav>
                    </div>
                </div>

            </div>
        </div>
    </div>
</header>

<br>

<section class="post_blog_bg primary-bg">
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <div class="col-md-8">

                    @foreach($posts as $post)
                    <article class="blog_post">
                        <h4> <a href="#"> {{ $post->title }} </a> </h4>
                        
                        <div class="blog_text">
                            <ul>
                                <li> <a href="#"> Postado em : {{ $post->created_at->format('d/m/Y') }} </a> </li>
                            </ul>
                        </div>
                        <div class="blog_post_img">
                            <a href="#"> <img src="{{ asset($post->image_url) }}" alt="image"> </a>
                        </div>
                        
                        <p> {{ $post->subtitle }} </p>

                        <a href="{{ url('post/' . $post->id) }}"> Clique aqui para continuar lendo <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                    
                    </article>
                    @endforeach

                      

                    <div class="next_page">
                        <ul class="page-numbers">
                            {{ $posts->links() }}
                        </ul>
                    </div>

                </div>  
                <style>
                .side_bar_heading h6:after {
                    display: none;
                }
                </style>

                {{-- <aside class="col-md-4">
                    <div class="side_blog_bg"                    
                    
                    <div class="sidebar_wrap">
                        <div class="side_bar_heading">
                            <h6>Outras postagens </h6>                           
                        </div>

                    @foreach($random_posts as $post)
                        <div class="recent-detail">
                            <div class="recent-image">
                                <a href="{{ url('post/'. $post->id) }}"> <img src="{{ asset($post->image_url) }}" alt="{{ $post->title }}"> </a>
                            </div> 

                            <div class="recent-text">

                                <h6> <a href="{{ url('post/'. $post->id) }}"> {{ $post->title }} </a> </h6>

                                <div class="blog_category side_category">
                                    <ul> 
                                        <li> <a href="#">Ler mais</a> </li>
                                    </ul>
                                </div>

                            </div>
                        </div>
                    @endforeach


                    </div>
                    

                </aside> --}}

                
                
            </div>
        </div>
    </div>
</section>


{{-- @include('page.services') --}}

@include('master.footer')

@include('master.js')

</body>

<!-- JS Plugins -->
</html>
