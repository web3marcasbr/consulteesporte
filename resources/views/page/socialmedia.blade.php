<section class="social-media">

    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <div class="footer_top">
                    <ul>
                        @include('master.socialmedia')
                        <li id="login-student"> <a style="font-family: roboto slab !important;" href="{{ url('/login') }}"> <i class="fa fa-sign-in" aria-hidden="true"> </i> Área do aluno </a> </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

</section>