@extends('master.navbar')

@section('home')
    @parent
@endsection

@section('team')
    @parent
@endsection

@section('pricing')
    @parent
@endsection

@section('blog')
    @parent
@endsection

@section('testimonial')
    @parent
@endsection

@section('portfolio')
    @parent
@endsection

@section('contact')
    @parent
@endsection

@section('user')

@endsection