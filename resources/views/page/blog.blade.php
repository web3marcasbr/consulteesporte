<section id="blog" class="primary-bg">
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <div class="section_heading">
                    <h2> @lang('post.title') </h2>

                    <h4> @lang('post.subtitle') </h4>
                </div>

                <!-- Posts -->
                @foreach ( $posts as $post )
                    @include('parts.blog_post')
                @endforeach

                <div class="col-md-12" style="text-align: center !important;">
                    <a href="{{ url('/blog') }}" class="btn btn-default btn-custom-2">Clique para ver todas as postagens
                    </a>
                </div>


            </div>
        </div>
    </div>
</section>