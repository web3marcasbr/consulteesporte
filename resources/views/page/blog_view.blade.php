<!DOCTYPE html>
<html>
<head>

    @include('master.metatags')

    <title> @lang('client.name') @yield('title')</title>

    @include('master.css')

</head>

<body>

<header class="navbar-fixed-top fixnav">
    <div class="container">
        <div class="row">
            <div class="header_top">
                <div class="col-md-2">
                    <div class="logo_img">
                        <a href="{{ url('/') }}"><img src="{{ asset('custom_img/logo.png') }}" alt="logoimage"></a>
                    </div>
                </div>

                <div class="col-md-10">
                    <div class="menu_bar">
                        <nav role="navigation" class="navbar navbar-default">
                            <div class="navbar-header">
                                <button id="menu_slide" aria-controls="navbar" aria-expanded="false"
                                        data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>

                            <div class="collapse navbar-collapse" id="navbar">

                                <ul class="nav navbar-nav">

                                    @section('home')
                                        <li><a href="{{ url('/#home') }}" class="js-target-scroll">@lang('home.navbar_name')</a></li>
                                    @show

                                    @section('team')
                                        <li><a href="{{ url('/#team') }}" class="js-target-scroll">@lang('team.navbar_name')</a></li>
                                    @show

                                    @section('pricing')
                                        <li><a href="{{ url('/#pricing') }}" class="js-target-scroll">@lang('price.navbar_name')</a>
                                        </li>
                                    @show

                                    @section('blog')
                                        <li><a href="{{ url('/#blog') }}" class="js-target-scroll">@lang('post.navbar_name')</a></li>
                                    @show

                                    @section('testimonial')
                                        <li><a href="{{ url('/#testimonial') }}" class="js-target-scroll">@lang('testimonial.navbar_name')</a></li>
                                    @show

                                    @section('portfolio')
                                        <li><a href="{{ url('/#portfolio') }}" class="js-target-scroll">@lang('partner.navbar_name')</a></li>
                                    @show

                                    @section('contact')
                                        <li><a href="{{ url('/#contact') }}" class="js-target-scroll">@lang('contact.navbar_name')</a>
                                        </li>
                                    @show

                                </ul>
                            </div>

                        </nav>
                    </div>
                </div>

            </div>
        </div>
    </div>
</header>

@include('page.post_view')

{{-- @include('page.services') --}}

@include('master.footer')

@include('master.js')

</body>

<!-- JS Plugins -->
</html>
