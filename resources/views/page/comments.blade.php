@foreach($comments as $comment)
    <div class="media">
        <div class="media-body">
            <h6 class="media-heading"> {{ $comment->title }}</h6>
            <p> {{ $comment->comment }} </p>
            <p><h6>
                <small>
                    {{ $comment->user->name or 'Anonimo' }} - {{ $comment->created_at }}

                    @if (isset($comment->user_id))
                        @if (Auth::user()->id == $comment->user->id || Auth::user()->admin == 1)
                            {{ Form::open(array('url' => 'comentario/' . $comment->id)) }}
                            {{ Form::hidden('_method', 'DELETE') }}
                            {{ Form::submit('Excluir comentário', array('class' => 'btn btn-danger btn-xs')) }}
                            {{ Form::close() }}
                        @endif
                    @endif

                </small>
            </h6></p>
        </div>
    </div>
@endforeach