<section id="team" class="primary-bg">
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <div class="section_heading">
                    <h2> @lang('team.title') </h2>

                    <h4> @lang('team.sub_title') </h4>
                </div>

                <!-- Member #1 -->
                <div class="team-box col-md-12">
                    <!-- Personal info -->
                    <div class="col-md-3">
                        <div class="member_detail">
                            <div class="member_img">
                                <img src="{{ asset('img/less/osmir.jpg') }}" alt="image">
                            </div>
                            <div class="member_name">
                                <p>  @lang('client.member#1-function') </p>
                            </div>
                        </div>
                    </div>

                    <!-- Text -->
                    <div class="col-md-9">
                        <div class="member_detail">
                            <div class="member_curriculo">
                                <h6> @lang('client.member#1-name') </h6>
                                <p>  @lang('client.member#1-description') </p>
                            </div>
                        </div>
                    </div>
                </div>

                
                <!-- Member #2 -->
                <div class="team-box col-md-12">

                    <!-- Text -->
                    <div class="col-md-9">
                        <div class="member_detail right">
                            <div class="member_curriculo">
                                <h6 id="client2name"> @lang('client.member#2-name') </h6>
                                <p id="text-member">  @lang('client.member#2-description') </p>
                            </div>
                        </div>
                    </div>

                    <!-- Personal info -->
                    <div class="col-md-3">
                        <div class="member_detail">
                            <div class="member_img">
                                <img src="{{ asset('img/less/andrea.jpg') }}" alt="image">
                            </div>
                            <div class="member_name">
                                <p>  @lang('client.member#2-function') </p>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
</section>

<section class="primary-bg" id="custom-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <div class="section_heading">
                    <h2> @lang('team.specialization_title') </h2>

                    <h4 id="custom"> @lang('team.specialization_subtitle') </h4>
                </div>

                <!-- First list -->
                <div class="col-md-6">
                    <div class="features_detail">
                        <ul>
                            <li>
                                <i class="fa fa-user" aria-hidden="true"></i>
                                <h5 class="text-team"  class="text-team"> @lang('team.specialization#1_name') </h5>
                                <p class="text-entire">@lang('team.specialization#1_description')</p>
                            </li>
                            <li>
                                <i class="fa fa-user" aria-hidden="true"></i>
                                <h5 class="text-team" > @lang('team.specialization#2_name') </h5>
                                <p class="text-entire">@lang('team.specialization#2_description')</p>
                            </li>
                            <li>
                                <i class="fa fa-user" aria-hidden="true"></i>
                                <h5 class="text-team" > @lang('team.specialization#3_name') </h5>
                                <p class="text-entire">@lang('team.specialization#3_description')</p>
                            </li>
                            <li>
                                <i class="fa fa-user" aria-hidden="true"></i>
                                <h5 class="text-team" > @lang('team.specialization#4_name') </h5>
                                <p class="text-entire">@lang('team.specialization#4_description')</p>
                            </li>
                        </ul>
                    </div>
                </div>

                <!-- Second list -->
                <div class="col-md-6">
                    <div class="features_detail">
                        <ul>
                            <li>
                                <i class="fa fa-user" aria-hidden="true"></i>
                                <h5 class="text-team" > @lang('team.specialization#5_name') </h5>
                                @lang('team.specialization#5_description')
                            </li>
                            <li id="assessoria">
                                <i class="fa fa-user" aria-hidden="true"></i>
                                <h5 class="text-team" > @lang('team.specialization#6_name') </h5>
                                <span class="description"> @lang('team.specialization#6_description')</span>
                            </li>
                            <li>
                                <i class="fa fa-user" aria-hidden="true"></i>
                                <h5 class="text-team" > @lang('team.specialization#7_name') </h5>
                                @lang('team.specialization#7_description')
                            </li>
                            <li>
                                <i class="fa fa-user" aria-hidden="true"></i>
                                <h5 class="text-team" > @lang('team.specialization#8_name') </h5>
                                @lang('team.specialization#8_description')
                            </li>
                        </ul>
                    </div>
                </div>

                <!-- Image
                <div class="col-md-6">
                    <div class="features_img pull-left">
                        <img src="images/features_img.png" alt="image">
                    </div>
                </div>
                -->

            </div>
        </div>
    </div>
</section>