<section id="contact" class="contact_bg">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section_heading section_heading_2">
                    <h2> @lang('contact.title') </h2>

                    <h4> @lang('contact.subtitle') </h4>
                    <style>
                        .min {
                            font-size: 24px;
                        }
                    </style>
                    <h4 class='min' style="padding-bottom: 5px"> <i class="fa fa-mobile" aria-hidden="true"> <span style="font-family: roboto slab !important;"> @lang('client.ddd') @lang('client.cellphone#1') </span> </i> </h4>
                    <h4 class='min' style="padding-bottom: 5px"> <i class="fa fa-envelope-o" aria-hidden="true"> <a style='font-family: roboto slab !important;' id="email-contact" href="mailto:{{ trans('client.email') }}"> @lang('client.email') </a> </i> </h4>

                    <style>
                        #email-contact, .fa-envelope-o {
                            color: hsl(42, 93%, 51%) !important;
                        }

                        #email-contact:hover {
                            color: white !important;
                        }

                        @media (max-width: 768px) {
                            .contact-box {
                                display: inline-block !important;
                                color: white !important;
                            }
                        }

                        .fa-mobile {
                            color: hsl(42, 93%, 51%) !important;
                        }

                        @media (min-width: 768px) {
                            .contact-box {
                                display: inline-flex !important;
                                color: white !important;
                            }
                        }
                    </style>

                    <div class="contact-box">


                    </div>

                </div>

                <div class="col-md-8 col-md-offset-2 contact-box">
                    {{--
                    <div class="contact_text">
                        <ul>
                            <li>
                                <span class="icon-box"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>
                                <h5> <a href="mailto:{{ trans('client.email') }}"> @lang('client.email') </a> </h5>
                            </li>

                            <li>
                                <span class="icon-box"><i class="fa fa-mobile" aria-hidden="true"></i></span>
                                <h5> @lang('client.ddd') @lang('client.cellphone#1') </h5>
                            </li>
                        </ul>
                    </div>
                    --}}

                </div>

                <div class="col-md-4 col-md-offset-2">

                    <div class="contact_form">
                        <div class="subscribe">
                            <h3> @lang('contact.form_title') </h3>

                            <h6> @lang('contact.form_subtitle') </h6>

                            <div class="subscribe_form">

                                {!! BootForm::open()->action(route('newsletter.store'))->multipart() !!}

                                <div class="form-group">
                                    <input type="email" class="form-control" name="email" id="exampleInputEmail1" placeholder="@lang('contact.email_field-placeholder')" required>
                                </div>

                                <div class="section_sub_btn">
                                    <button type="submit" class="btn btn-default" type="submit">  @lang('contact.button_submit') </button>
                                </div>

                                {!! BootForm::close() !!}

                            </div>

                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <style>
                        iframe {
                            top: 0;
                            left: 0;
                            width: 100%;
                        }
                    </style>
                    <iframe src="https://open.spotify.com/user/consulteesporte/playlist/6fceZ2TBhoPUHmUvWfXsrK" width="350" height="466" frameborder="0" allowtransparency="true"></iframe>
                </div>

            </div>
        </div>
    </div>
</section>
