<style>
    .modal-front-page{
        margin-top: 120px;
    }

    .modal-backdrop {
        z-index: -1;
    }

    #testimonial-modal-btn {
        transition: color 0.5s ease;
    }

    #testimonial-modal-btn:hover {
        color: white;
        cursor: pointer;
    }

    .testi-text {
        width: 100%;
    }
</style>

<section id="testimonial" class="testimonial_bg">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section_heading section_heading_2">
                    <h2> @lang('testimonial.title') </h2>
                    <h6 id="testimonial-modal-btn" data-toggle="modal" data-target="#testimonialModal"> @lang('testimonial.testimonial-message') </h6>

                    {{-- <h4> @lang('testimonial.subtitle') </h4> --}}
                </div>

                <style>
                    .customNavigation {
                        border: solid 1px white;
                        width: 200px;
                    }
                </style>

                <div class="customNavigation center-block">
                    <a class="btn prev"><i class="fa fa-step-backward" aria-hidden="true"></i> Anterior</a>
                    <!--<a class="btn stop"><i class="fa fa-stop" aria-hidden="true"></i> Parar</a>
                    <a class="btn play"><i class="fa fa-play" aria-hidden="true"></i> Continuar</a>-->
                    <a class="btn next"><i class="fa fa-step-forward" aria-hidden="true"></i> Próximo</a>
                </div>

                <div id="owl-demo" class="owl-carousel owl-theme">
                    @foreach( $testimonials as $testimonial)
                    <div class="item">

                        <div class="testi_detail">
                            <div class="testi-text">
                                <p>
                                    <i class="fa fa-quote-left" aria-hidden="true"></i>
                                    {{ $testimonial->text }}
                                    <i class="fa fa-quote-right" aria-hidden="true"></i>
                                </p>
                                <div class="testi-signature">
                                    <h6 class="second_color">- {{ $testimonial->name or $testimonial->user->name }} </h6>
                                    <p class="white"><small><strong> {{ $testimonial->city or 'Cidade ou empresa' }} </strong></small></p>
                                </div>
                            </div>
                        </div>

                    </div>
                    @endforeach
                </div>

                <style>
                    .customNavigation a:hover {
                        color: white;
                    }
                </style>

                <!-- Modal -->
                <div class="modal fade modal-front-page" id="testimonialModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        {{ Form::open(array('action' => 'IndexController@TestimonialStore')) }}
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel"> @lang('testimonial.modal-title') </h4>
                                </div>
                                <div class="modal-body">

                                    {{ Form::label('text-label', trans('testimonial.form-text')) }}
                                    <textarea name='text' class="form-control" rows="5" required></textarea>

                                    {{ Form::label('text-label', trans('testimonial.form-name')) }}
                                    <input name='name' type="text" class="form-control" placeholder="{{ trans('testimonial.form-name-placeholder') }}" required>

                                    {{ Form::label('text-label', 'Qual a sua cidade ou empresa?') }}
                                    <input name='city' type="text" class="form-control" placeholder="" required>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal"> @lang('post.button-cancel') </button>

                                    {{ Form::submit(trans('testimonial.form-btn-submit'), array('class' => 'btn btn-primary')) }}

                                </div>
                            </div>
                        {{ Form::close() }}

                    </div>
                </div>

            </div>
        </div>
    </div>
</section>