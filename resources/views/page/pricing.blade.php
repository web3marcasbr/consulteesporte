<style>
    .modalfix {
        margin-top: 70px !important;
    }

    .modal-backdrop {
        z-index: -1 !important;
    }
</style>

<section id="pricing" class="price_table_bg">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section_heading section_heading_2">
                    <h2> @lang('price.title') </h2>
                    <h4> @lang('price.subtitle') </h4>
                </div>

                @if (count($prices) == 3)
                    <link href="{{ asset('css/center-bootstrap.css') }}" rel="stylesheet">
                    <div class="community-images">
                        @else
                            <div class="">
                                @endif

                                @foreach( $prices as $price )
                                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                        <div class="pricing table-1">
                                            <!--
                                                <div class="discount">
                                                    <p> Save 10% </p>
                                                </div>
                                                -->
                                            <h3> {{ $price->name }} </h3>
                                            @if ( $price->featured == 0)
                                                <div class="price_month">
                                                    @else
                                                        <div class="price_month_m">
                                                            @endif
                                                            <span class="round">
                    <h3 class="price"> R$ {{ $price->price }} </h3>
                    <span>
                        <p> {{ $price->price_complement }} </p>
                    </span>
                </span>
                                                        </div>
                                                        <ul>
                                                            <li> {{ $price->description }} </li>
                                                        </ul>
                                                        <div class="section_sub_btn">
                                                            <button type="button" class="btn btn-default"
                                                                    data-toggle="modal"
                                                                    data-target="{{ '#exampleModal_' . $price->id }}"
                                                                    data-whatever="@.{{ $price->id }}">@lang ('price.index-buy')
                                                            </button>
                                                        </div>
                                                </div>
                                        </div>

                                        <div class="modal fade modalfix" id="{{ 'exampleModal_' . $price->id }}"
                                             tabindex="-1" role="dialog"
                                             aria-labelledby="exampleModalLabel">
                                            <div class="modal-dialog" role="document">
                                                {{ Form::open(array('action' => 'IndexController@TestimonialStore')) }}
                                                <div class="modal-content">
                                                    <div class="modal-body">
                                                        {{ Form::label('Assunto') }}
                                                        {{ Form::text('subject', 'Quero contratar o plano: ' . $price->name, ['class' => 'form-control']) }}
                                                        {{ Form::label('text-label', trans('testimonial.form-name')) }}
                                                        <input name='name' type="text" class="form-control"
                                                               placeholder="{{ trans('testimonial.form-name-placeholder') }}"
                                                               required>
                                                        {{ Form::label('text-label', 'Telefone de contato') }}
                                                        <input name='cellphone' type="text" class="form-control"
                                                               placeholder="Ex: (19) 1954-7482" required>
                                                        {{ Form::label('text-label', 'Email de contato') }}
                                                        <input name='email' type="text" class="form-control" required>
                                                        {{ Form::label('text-label', 'Deseja escrever ou perguntar algo a mais?') }}
                                                        <textarea name='more' class="form-control" rows="5"
                                                                  required></textarea>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default"
                                                                data-dismiss="modal">Fechar
                                                        </button>
                                                        <button type="button" class="btn btn-primary">Enviar email
                                                        </button>
                                                    </div>
                                                </div>
                                                {{ Form::close() }}
                                            </div>
                                        </div>


                                        @endforeach
                                    </div>
                            </div>
                    </div>

                    <div class="container">
                        <div class="row col-md-12">
                            <div class="col-md-12" style="text-align: center !important;">
                                <a href="mailto:contato@consulteesporte.com.br"
                                   class="btn btn-default btn-custom-2"
                                >Personalize o seu plano <br> clique aqui!
                                </a>
                            </div>
                        </div>
                    </div>
</section>