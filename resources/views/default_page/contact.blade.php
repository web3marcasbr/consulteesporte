<section id="contact" class="contact_bg">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section_heading section_heading_2">
                    <h2> Contact Us </h2>

                    <h4>Let drop a line to us & we will be in touch soon. Here goes some simple dummy text. Lorem Ipsum is simply dummy text </h4>
                </div>

                <div class="col-md-6">
                    <div class="contact_form">
                        <form>
                            <div class="form-group">
                                <label >Full Name : <span> *</span></label>
                                <input type="email" class="form-control" id="exampleInputEmail1" >
                            </div>

                            <div class="form-group">
                                <label >Email Address : <span> *</span></label>
                                <input type="text" class="form-control" id="exampleInputPassword1" >
                            </div>

                            <div class="form-group">
                                <label>Message <span> *</span></label>
                                <textarea class="form-textarea" rows="3"></textarea>
                            </div>

                            <div class="section_sub_btn">
                                <button class="btn btn-default" type="submit">  Send Message</button>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="contact_text">
                        <ul>
                            <li>
                                <span><i class="fa fa-home" aria-hidden="true"></i></span>
                                <h5> 1234 Street Name, City Name, United States</h5>
                            </li>

                            <li>
                                <span><i class="fa fa-envelope-o" aria-hidden="true"></i></span>
                                <h5> contact@domain.com </h5>
                            </li>

                            <li>
                                <span><i class="fa fa-phone" aria-hidden="true"></i></span>
                                <h5> (123) 123-45678 </h5>
                            </li>

                            <li>
                                <span><i class="fa fa-fax" aria-hidden="true"></i></span>
                                <h5> (123) 123-45678 </h5>
                            </li>
                        </ul>

                    </div>
                </div>


            </div>
        </div>
    </div>
</section>

<section class="primary-bg">
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <div class="col-md-4">
                    <div class="subscribe">
                        <h3> Stay informed with our newsletter</h3>

                        <h6> Subscribe to our email newsletter for useful tips and resources. </h6>

                        <div class="subscribe_form">
                            <form>
                                <div class="form-group">
                                    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email Address" >
                                </div>
                            </form>

                            <div class="section_sub_btn">
                                <button class="btn btn-default" type="submit">  Subscribe </button>
                            </div>
                        </div>

                    </div>


                </div>

                <div class="col-md-4">
                    <div class="workng_img">
                        <img src="images/contact_img.png" alt="image">
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="subscribe">
                        <h3> Download App Now </h3>

                        <h6> Select your device platform and get download started </h6>

                        <div class="section_btn">
                            <button class="btn btn-default" type="submit"> <i class="fa fa-apple" aria-hidden="true"></i> App Store</button>

                            <span><button class="btn btn-default" type="submit"><i class="fa fa-android" aria-hidden="true"></i> Play Store</button></span>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>