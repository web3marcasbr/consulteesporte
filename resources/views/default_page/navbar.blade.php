<header class="navbar-fixed-top">
    <div class="container">
        <div class="row">
            <div class="header_top">
                <div class="col-md-2">
                    <div class="logo_img">
                        <a href="#"><img src="images/logo.png" alt="logoimage"></a>
                    </div>
                </div>

                <div class="col-md-10">
                    <div class="menu_bar">
                        <nav role="navigation" class="navbar navbar-default">
                            <div class="navbar-header">
                                <button id="menu_slide" aria-controls="navbar" aria-expanded="false"
                                        data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>

                            <div class="collapse navbar-collapse" id="navbar">

                                <ul class="nav navbar-nav">
                                    <li><a href="#home" class="js-target-scroll">@lang('home.navbar_name')</a></li>
                                    <li><a href="#services" class="js-target-scroll">@lang('services.navbar_name')</a></li>
                                    <li><a href="#portfolio" class="js-target-scroll">@lang('navbar.portfolio_home')</a></li>
                                    <li><a href="#pricing" class="js-target-scroll">@lang('price.navbar_name')</a></li>
                                    <li><a href="#team" class="js-target-scroll">@lang('team.navbar_name')</a></li>
                                    <li><a href="#testimonial" class="js-target-scroll">@lang('testimonial.navbar_name')</a></li>
                                    <li><a href="#blog" class="js-target-scroll">@lang('blog.navbar_name')</a></li>
                                    <li><a href="#contact" class="js-target-scroll">@lang('contact.navbar_name')</a></li>
                                </ul>
                            </div>

                        </nav>
                    </div>
                </div>

            </div>
        </div>
    </div>
</header>