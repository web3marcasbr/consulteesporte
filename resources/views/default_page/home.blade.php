<!--
<section id="home" class="top_banner_bg secondary-bg">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="top_banner">

                </div>

                <div class="col-md-6">
                    <div class="present">
                        <h1> @lang('home.main_sentence') </h1>

                        <!--<h5> Available on <span> App Store </span> and <b> Play Store</b> </h5>
                        <h5> @lang('home.main_sub_sentence') </h5>

                        <div class="section_btn">
                            <a href="#"> <button class="btn btn-default" type="submit"> <i class="fa fa-apple" aria-hidden="true"></i> @lang('home.button#1') </button> </a>

                            <span> <a href="#"> <button class="btn btn-default" type="submit"><i class="fa fa-android" aria-hidden="true"></i> @lang('home.button#2')</button> </a> </span>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="present_img">
                        <img src="images/image-1.png" alt="image">
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>

<section class="primary-bg">
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <div class="section_heading">
                    <h2> @lang('home.sentence') </h2>

                    <h4> @lang('home.sub_sentence') </h4>
                </div>

                 First list
                <div class="col-md-6">
                    <div class="features_detail">
                        <ul>
                            <li>
                                <i class="fa fa-user" aria-hidden="true"></i>
                                <h5> @lang('home.product#1_name') </h5>
                                @lang('home.product#1_description')
                            </li>
                            <li>
                                <i class="fa fa-user" aria-hidden="true"></i>
                                <h5> @lang('home.product#2_name') </h5>
                                @lang('home.product#2_description')
                            </li>
                            <li>
                                <i class="fa fa-user" aria-hidden="true"></i>
                                <h5> @lang('home.product#3_name') </h5>
                                @lang('home.product#3_description')
                            </li>
                            <li>
                                <i class="fa fa-user" aria-hidden="true"></i>
                                <h5> @lang('home.product#4_name') </h5>
                                @lang('home.product#4_description')
                            </li>
                        </ul>
                    </div>
                </div>

                <!-- Second list
                <div class="col-md-6">
                    <div class="features_detail">
                        <ul>
                            <li>
                                <i class="fa fa-user" aria-hidden="true"></i>
                                <h5> @lang('home.product#5_name') </h5>
                                @lang('home.product#5_description')
                            </li>
                            <li>
                                <i class="fa fa-user" aria-hidden="true"></i>
                                <h5> @lang('home.product#6_name') </h5>
                                @lang('home.product#6_description')
                            </li>
                            <li>
                                <i class="fa fa-user" aria-hidden="true"></i>
                                <h5> @lang('home.product#7_name') </h5>
                                @lang('home.product#7_description')
                            </li>
                            <li>
                                <i class="fa fa-user" aria-hidden="true"></i>
                                <h5> @lang('home.product#8_name') </h5>
                                @lang('home.product#8_description')
                            </li>
                        </ul>
                    </div>
                </div>

                <!-- Image
                <div class="col-md-6">
                    <div class="features_img pull-left">
                        <img src="images/features_img.png" alt="image">
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>