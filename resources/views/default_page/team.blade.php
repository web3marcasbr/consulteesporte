<section id="team" class="primary-bg">
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <div class="section_heading">
                    <h2> Team Members </h2>

                    <h4> We're the best professionals in this. Here goes some simple dummy text. Lorem Ipsum is simply dummy text </h4>
                </div>

                <div class="col-md-3">
                    <div class="member_detail">
                        <div class="member_img">
                            <img src="images/member_1.png" alt="image">
                        </div>
                        <div class="member_name">
                            <h5> John Capone</h5>
                            <p> Web Art Director</p>
                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="member_detail">
                        <div class="member_img">
                            <img src="images/member_2.png" alt="image">
                        </div>
                        <div class="member_name">
                            <h5> Marlon Leend</h5>
                            <p> Head Phographer</p>
                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="member_detail">
                        <div class="member_img">
                            <img src="images/member_3.png" alt="image">
                        </div>
                        <div class="member_name">
                            <h5> Robert Son</h5>
                            <p> Marketing Director </p>
                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="member_detail">
                        <div class="member_img">
                            <img src="images/member_4.png" alt="image">
                        </div>
                        <div class="member_name">
                            <h5> John Capone</h5>
                            <p> Web Art Director</p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>