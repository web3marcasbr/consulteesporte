<section id="blog" class="primary-bg">
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <div class="section_heading">
                    <h2> @lang('post.exclusive-title') </h2>

                    <h4> @lang('post.exclusive-subtitle') </h4>
                </div>

                <!-- Posts -->
                @foreach ( $posts as $post )
                    @include('parts.blog_post')
                @endforeach

            </div>
        </div>
    </div>
</section>