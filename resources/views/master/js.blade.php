<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/interface.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $("#menu_slide").click(function(){
            $("#navbar").slideToggle('normal');
        });
    });
</script>

<!--Menu Js Right Menu-->
<script type="text/javascript">
    $(document).ready(function(){
        $('#navbar > ul > li:has(ul)').addClass("has-sub");
        $('#navbar > ul > li > a').click(function() {
            var checkElement = $(this).next();
            $('#navbar li').removeClass('dropdown');
            $(this).closest('li').addClass('dropdown');
            if((checkElement.is('ul')) && (checkElement.is(':visible'))) {
                $(this).closest('li').removeClass('dropdown');
                checkElement.slideUp('normal');
            }
            if((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
                $('#navbar ul ul:visible').slideUp('normal');
                checkElement.slideDown('normal');
            }
            if (checkElement.is('ul')) {
                return false;
            } else {
                return true;
            }
        });
    });
    <!--end-->
</script>
<script type="text/javascript">
    $("#navbar").on("click", function(event){
        event.stopPropagation();
    });
    $(".dropdown-menu").on("click", function(event){
        event.stopPropagation();
    });
    $(document).on("click", function(event){
        $(".dropdown-menu").slideUp('normal');
    });

    $(".navbar-header").on("click", function(event){
        event.stopPropagation();
    });
    $(document).on("click", function(event){
        $("#navbar").slideUp('normal');
    });
</script>

<script>
    $(document).ready(function() {

        var owl = $("#owl-demo");

        owl.owlCarousel({
            items : 3, //10 items above 1000px browser width
            itemsDesktop : [1000,3], //5 items between 1000px and 901px
            itemsDesktopSmall : [900,2], // betweem 900px and 601px
            itemsTablet: [600,1], //2 items between 600 and 0
            itemsMobile : false, // itemsMobile disabled - inherit from itemsTablet option
            autoPlay : true
        });

        // Custom Navigation Events
        $(".next").click(function(){
            owl.trigger('owl.next');
        })
        $(".play").click(function(){
            owl.trigger('owl.play',5000); //owl.play event accept autoPlay speed as second parameter
        })

    });


</script>