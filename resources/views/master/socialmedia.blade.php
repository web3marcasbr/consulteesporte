<li> <a target="_blank" href=" {{ trans('client.facebook-url') }} "> <i class="fa fa-facebook" aria-hidden="true" ></i> </a> </li>
<li> <a target="_blank" href=" {{ trans('client.spotify-url') }} "> <i class="fa fa-spotify" aria-hidden="true" ></i> </a> </li>
<li> <a target="_blank" href=" {{ trans('client.linkedin-url') }} "> <i class="fa fa-linkedin" aria-hidden="true" ></i> </a> </li>
<li> <a target="_blank" href=" {{ trans('client.youtube-url') }} "> <i class="fa fa-youtube-square" aria-hidden="true" ></i> </a> </li>
<li> <a target="_blank" href=" {{ trans('client.instagram-url') }} "> <i class="fa fa-instagram" aria-hidden="true" ></i> </a> </li>