<header class="navbar-fixed-top fixnav">
    <div class="container">
        <div class="row">
            <div class="header_top">
                <div class="col-md-2">
                    <div class="logo_img">
                        <a href=""><img src="{{ asset('custom_img/logo.png') }}" alt="logoimage"></a>
                    </div>
                </div>

                <div class="col-md-10">
                    <div class="menu_bar">
                        <nav role="navigation" class="navbar navbar-default">
                            <div class="navbar-header">
                                <button id="menu_slide" aria-controls="navbar" aria-expanded="false"
                                        data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>

                            <div class="collapse navbar-collapse" id="navbar">

                                <ul class="nav navbar-nav">

                                    @section('home')
                                        <li><a href="#home" class="js-target-scroll">@lang('home.navbar_name')</a></li>
                                    @show

                                    @section('team')
                                        <li><a href="#team" class="js-target-scroll">@lang('team.navbar_name')</a></li>
                                    @show

                                    @section('pricing')
                                        <li><a href="#pricing" class="js-target-scroll">@lang('price.navbar_name')</a>
                                        </li>
                                    @show

                                    @section('blog')
                                        <li><a href="#blog" class="js-target-scroll">@lang('post.navbar_name')</a></li>
                                    @show

                                    @section('testimonial')
                                        <li><a href="#testimonial"
                                               class="js-target-scroll">@lang('testimonial.navbar_name')</a></li>
                                    @show

                                    @section('portfolio')
                                        <li><a href="#portfolio"
                                               class="js-target-scroll">@lang('partner.navbar_name')</a></li>
                                    @show

                                    @section('contact')
                                        <li><a href="#contact" class="js-target-scroll">@lang('contact.navbar_name')</a>
                                        </li>
                                    @show

                                    @section('user')
                                        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                @if ( isset( Auth::user()->name ))
                                                    {{ Auth::user()->name }}
                                                    <i class="fa fa-bars" aria-hidden="true"></i> </a>
                                            <ul class="dropdown-menu">
                                                <li><a href="#"><i class="fa fa-sign-out" aria-hidden="true"></i> Sair</a></li>
                                            </ul>
                                            @else
                                                <span>aluno</span>
                                                <i class="fa fa-bars" aria-hidden="true"></i> </a>
                                                <ul class="dropdown-menu">
                                                    <li><a href="#"><i class="fa fa-sign-out" aria-hidden="true"></i> Entrar</a></li>
                                                </ul>
                                            @endif
                                        </li>
                                    @show

                                </ul>
                            </div>

                        </nav>
                    </div>
                </div>

            </div>
        </div>
    </div>
</header>