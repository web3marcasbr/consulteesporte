<footer class="third-bg">
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <div class="footer_top">
                    <h4> @lang('footer.social_media-title')  </h4>

                    <ul>
                        @include('master.socialmedia')
                    </ul>
                </div>

            </div>
        </div>
    </div>

    <div class="footer_bottom fourth-bg">
        <!-- Keep Footer Credit Links Intact -->
        <p> 2016 &copy; <a href="marcasbrasil.com.br">Marcas Brasil Com & Mkt</a> </p>
        <a href="#" class="backtop"> ^ </a>
    </div>

</footer>