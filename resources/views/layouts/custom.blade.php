
<!DOCTYPE html>
<html>
<head>
    @include('master.metatags')

    <title> @lang('client.name') @yield('title')</title>

    @include('master.css')

            <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
                'csrfToken' => csrf_token(),
        ]); ?>
    </script>

</head>

<body>

@include('auth.navbar')

@yield('content')

{{-- @include('page.services') --}}

@include('master.footer')

@include('master.js')

</body>

<!-- JS Plugins -->
</html>

