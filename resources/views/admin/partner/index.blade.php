@extends('admin.master')

@section('title', trans('partner.title-index'))
@section('subtitle', trans('partner.subtitle-index'))
@section('breadcrumb', Breadcrumbs::render('partner'))

@section('content')

    @if( !isset($partners) )
        <div class="alert alert-info" role="alert">
            <strong>Ops!</strong> Parece que você não tem nenhum postagem cadastrada ainda <a href="post/create" class="alert-link">clique aqui</a> para criar uma nova.
        </div>
    @endif

    <div class="col-md-12">
        <p> @include('admin.partner.button.create') </p>
    </div>

    @foreach($partners as $partner)
        <div class="col-md-6 col-md-6">
            <div class="thumbnail media">
                <div class="media-left">
                    <img class="thumb xsmall" src="{{ asset( $partner->image_id ) }}" alt="...">
                </div>
                <div class="media-body">
                    <h4 class="media-heading">{{ $partner->nome }}</h4>
                    <p></p><a target="_blank" href="/{{ $partner->link }}"> {{ $partner->link }} </a></p>

                    <div>

                        @include('admin.partner.button.edit')

                    </div>
                </div>
            </div>
        </div>

    @endforeach

@endsection