@extends('admin.master')

@section('title', trans('partner.title-edit'))
@section('subtitle', trans('partner.subtitle-edit'))
@section('breadcrumb', Breadcrumbs::render('partner_edit', $partner))

@section('content')
    <div class="col-lg-12">

        {!! BootForm::open()->action( route('parceria.update', $partner->id) )->multipart()->put() !!}
        {!! BootForm::bind($partner) !!}

        {!! BootForm::text(trans('partner.form-name'), 'nome')->placeholder(trans('partner.form-name-placeholder'))->required() !!}
        {!! BootForm::text(trans('partner.form-link'), 'link')->placeholder(trans('partner.form-link-placeholder')) !!}
        {!! BootForm::file(trans('partner.form-image_edit'), 'image')->helpblock(trans('partner.form-image_edit-help')) !!}

        @include('admin.partner.button.cancel')
        {!! BootForm::submit(trans('partner.button-edit'))->addclass('btn btn-primary') !!}

        {!! BootForm::close() !!}

        <br>
        @include('admin.partner.button.delete')

    </div>
@endsection