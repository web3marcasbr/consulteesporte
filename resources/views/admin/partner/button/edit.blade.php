<a class="btn btn-small btn-default" href="{{ URL::to('admin/parceria/' . $partner->id . '/edit') }}">
    <i class="fa fa-pencil" aria-hidden="true"></i>
    @lang('partner.button-edit') </a>