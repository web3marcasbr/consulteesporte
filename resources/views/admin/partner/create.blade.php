@extends('admin.master')

@section('title', trans('partner.title-create'))
@section('subtitle', trans('partner.subtitle-create'))
@section('breadcrumb', Breadcrumbs::render('partner_create'))

@section('content')
    <div class="col-lg-12">

        {!! BootForm::open()->action(route('parceria.store'))->multipart() !!}

        {!! BootForm::text(trans('partner.form-name'), 'nome')->placeholder(trans('partner.form-name-placeholder'))->required() !!}
        {!! BootForm::text(trans('partner.form-link'), 'link')->placeholder(trans('partner.form-link-placeholder')) !!}
        {!! BootForm::file(trans('partner.form-upload'), 'image') !!}

        {!! BootForm::submit(trans('partner.button-create'))->addclass('btn btn-primary') !!}

        {!! BootForm::close() !!}

    </div>
@endsection