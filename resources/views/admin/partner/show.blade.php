@extends('admin.master')

@section('title', trans('partner.title-show'))
@section('subtitle', trans('partner.subtitle-show'))
@section('breadcrumb', Breadcrumbs::render('partner_show', $partner))

@section('content')
    <div class="row">
        <div class="col-md-12">

            @include('components.alert')

            <div class="col-xs-12 col-sm-4 col-md-6 col-lg-6">
                <span class="thumbnail">
                    <img src="{{ asset( $partner->image_url) }}" alt="...">
                </span>
            </div>

            <div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">
                <h2> {{ $partner->title }} </h2>
                <h4> {{ $partner->subtitle }}</h4>
                {!! $partner->text !!}
            </div>

        </div>

        <div class="col-md-12">

            <a class="btn btn-small btn-default" href="{{ URL::to('admin/post/') }}"> @lang('partner.button-back') </a>

            @include('admin.partner.button.edit')

            @include('admin.partner.button.delete')
        </div>
    </div>
@endsection
