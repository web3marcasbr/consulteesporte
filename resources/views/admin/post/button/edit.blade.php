<a class="btn btn-small btn-default" href="{{ URL::to('admin/post/' . $post->id . '/edit') }}">
    <i class="fa fa-pencil" aria-hidden="true"></i>
    @lang('post.button-edit') </a>