@extends('admin.master')

@section('title', trans('post.title-create'))
@section('subtitle', trans('post.subtitle-create'))
@section('breadcrumb', Breadcrumbs::render('post_create'))

<script src="//cdn.ckeditor.com/4.5.11/full/ckeditor.js"></script>

@section('content')
    <div class="col-lg-12">

        {!! BootForm::open()->action(route('post.store'))->multipart() !!}


        {!! BootForm::text(trans('post.form-title'), 'title')->placeholder(trans('post.form-title-placeholder'))->required() !!}
        {!! BootForm::text(trans('post.form-subtitle'), 'subtitle')->placeholder(trans('post.form-subtitle-placeholder'))->required() !!}
        {{-- BootForm::text(trans('post.form-text'), 'text')->placeholder('Instalar o CKeditor')->required() --}}
        <textarea name="text"></textarea>
        <script>
            CKEDITOR.replace( 'text' );
        </script>
        {!! BootForm::file(trans('post.form-upload'),'image') !!}
        {!! BootForm::select(trans('post.form-private'), 'private')->options(['1' => 'Sim', '0' => 'Não'])->select('0')->required() !!}
        {!! BootForm::submit(trans('post.form-btn-submit')) !!}

        {!! BootForm::close() !!}

    </div>
@endsection