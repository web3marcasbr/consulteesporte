@extends('admin.master')

@section('title', trans('post.title-edit'))
@section('subtitle', trans('post.subtitle-edit'))
@section('breadcrumb', Breadcrumbs::render('post_edit', $post))

<script src="//cdn.ckeditor.com/4.5.11/full/ckeditor.js"></script>

@section('content')
    <div class="col-lg-12">

        {!! BootForm::open()->action( route('post.update', $post->id) )->multipart()->put() !!}
        {!! BootForm::bind($post) !!}

        {!! BootForm::text(trans('post.form-title'), 'title')->placeholder(trans('post.form-title-placeholder'))->required() !!}
        {!! BootForm::text(trans('post.form-subtitle'), 'subtitle')->placeholder(trans('post.form-subtitle-placeholder'))->required() !!}
        <textarea name="text" >{{ $post->text }}</textarea>
        <script>
            CKEDITOR.replace( 'text' );
        </script>

        {!! BootForm::file('Deseja trocar a imagem do post?','image')->helpblocK('Caso não deseje, ignore esse campo') !!}

        {!! BootForm::select(trans('post.form-private'), 'private')->options(['1' => 'Sim', '0' => 'Não'])->select('0')->required() !!}

        @include('admin.post.button.cancel')
        {!! BootForm::submit('Editar postagem')->addClass('btn btn-primary') !!}

        {!! BootForm::close() !!}

        <br>
        @include('admin.post.button.delete')

    </div>
@endsection