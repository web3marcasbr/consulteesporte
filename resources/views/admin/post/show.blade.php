@extends('admin.master')

@section('title', trans('post.title-show'))
@section('subtitle', trans('post.subtitle-show'))
@section('breadcrumb', Breadcrumbs::render('post_show', $post))

@section('content')
    <div class="row">
        <div class="col-md-12">

            @include('components.alert')

            <div class="col-md-12">
                @if ($post->exclusive != 0)
                <div class="alert alert-warning" role="alert">
                    <!--<strong>Warning!</strong>--> Esse conteúdo é <strong> privado</strong>. Disponível apenas para alunos.
                </div>
                @else
                <div class="alert alert-info" role="alert">
                    <!--<strong>Warning!</strong>--> Esse conteúdo é <strong> público</strong>. Disponível para todos.
                </div>
                @endif
            </div>

            <div class="col-xs-12 col-sm-4 col-md-6 col-lg-6">
                <span class="thumbnail">
                    <img src="{{ asset( $post->image_url) }}" alt="...">
                </span>
            </div>

            <div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">
                <h2> {{ $post->title }} </h2>
                <h4> {{ $post->subtitle }}</h4>
                {!! $post->text !!}
            </div>

        </div>

        <div class="col-md-12">

            <a class="btn btn-small btn-default" href="{{ URL::to('admin/post/') }}"> @lang('post.button-back') </a>

            @include('admin.post.button.edit')

            @include('admin.post.button.delete')
        </div>
    </div>
@endsection
