@extends('admin.master')

@section('title', trans('post.title-index'))
@section('subtitle', trans('post.subtitle-index'))
@section('breadcrumb', Breadcrumbs::render('post'))

@section('content')

    @if( !isset($posts) )
        <div class="alert alert-info" role="alert">
            <strong>Ops!</strong> Parece que você não tem nenhum postagem cadastrada ainda <a href="post/create" class="alert-link">clique aqui</a> para criar uma nova.
        </div>
    @endif

    <div class="col-md-12">
        <p> @include('admin.post.button.create') </p>
    </div>

    @foreach($posts as $post)
        <div class="ro">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="thumbnail media">
                    <div class="media-left">
                        <img class="thumb xmedium" src="{{ asset( $post->image_url) }}" alt="...">
                    </div>
                    <div class="media-body">
                        <h3> {{ $post->title }}</h3>
                        <p> {{ $post->subtitle }} </p>

                        <div>

                            @include('admin.post.button.show')

                            @include('admin.post.button.edit')

                        </div>
                    </div>
                </div>
            </div>
        </div>

    @endforeach

@endsection