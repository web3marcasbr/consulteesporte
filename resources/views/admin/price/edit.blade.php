@extends('admin.master')

@section('title', trans('post.title-edit'))
@section('subtitle', trans('post.subtitle-edit'))
@section('breadcrumb', Breadcrumbs::render('price_edit', $price))

@section('content')
    <div class="col-lg-12">

        {!! BootForm::open()->action( route('preco.update', $price->id) )->put() !!}
        {!! BootForm::bind($price) !!}

        {!! BootForm::text(trans('price.form-name'), 'name')->placeholder(trans('price.form-name-placeholder'))->required() !!}

        {!! BootForm::text(trans('price.form-price'), 'price')->placeholder(trans('price.form-price-placeholder'))->required() !!}

        {!! BootForm::text(trans('price.form-complement'), 'price_complement')->placeholder(trans('price.form-complement-placeholder'))->required()
                ->helpblock(trans('price.form-complement-help')) !!}

        {!! BootForm::text(trans('price.form-description'), 'description')->placeholder(trans('price.form-description-placeholder'))->required() !!}

        {!! BootForm::select(trans('price.form-featured'), 'featured')->options(['1' => 'Sim', '0' => 'Não'])->select('0')
                ->helpblock(trans('price.form-featured-help'))->required() !!}

        @include('admin.price.button.cancel')

        {!! BootForm::submit(trans('price.form-button-edit'))->addclass('btn btn-primary') !!}

        {!! BootForm::close() !!}

        <br>
        @include('admin.price.button.delete')

    </div>
@endsection