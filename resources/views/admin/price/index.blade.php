@extends('admin.master')

@section('title', trans('price.title-index'))
@section('subtitle', trans('price.subtitle-index'))
@section('breadcrumb', Breadcrumbs::render('price'))

@section('content')

    @if( !isset($prices) )
        <div class="alert alert-info" role="alert">
            <strong>Ops!</strong> Parece que você não tem nenhuma oferta cadastrada ainda <a href="{{ route("preco.create") }}"
                                                                                              class="alert-link">clique
                aqui</a> para criar uma nova.
        </div>
    @endif

    <div class="col-md-12">
        <p> @include('admin.price.button.create') </p>
    </div>

    @foreach($prices as $price)

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="thumbnail">
                <div class="caption">
                    <h2> {{ $price->name }}</h2>
                    <h3> {{ $price->price }} {{ $price->price_complement }}</h3>
                    <p> @include('admin.price.components.label') {{ $price->description }}</p>

                    <div>

                        @include('admin.price.button.edit')

                    </div>

                </div>
            </div>
        </div>

    @endforeach

@endsection