@extends('admin.master')

@section('title', trans('price.title-create'))
@section('subtitle', trans('price.subtitle-create'))
@section('breadcrumb', Breadcrumbs::render('price_create'))

@section('content')
    <div class="col-lg-12">

        {!! BootForm::open()->action(route('preco.store'))->multipart() !!}
        
        {!! BootForm::text(trans('price.form-name'), 'name')->placeholder(trans('price.form-name-placeholder'))->required() !!}
        
        {!! BootForm::text(trans('price.form-price'), 'price')->placeholder(trans('price.form-price-placeholder'))->required() !!}
        
        {!! BootForm::text(trans('price.form-complement'), 'price_complement')->placeholder(trans('price.form-complement-placeholder'))->required()
                ->helpblock(trans('price.form-complement-help')) !!}

        {!! BootForm::text(trans('price.form-description'), 'description')->placeholder(trans('price.form-description-placeholder'))->required() !!}
        
        {!! BootForm::select(trans('price.form-featured'), 'featured')->options(['1' => 'Sim', '0' => 'Não'])->select('0')
                ->helpblock(trans('price.form-featured-help'))->required() !!}

        @include('admin.price.button.cancel')
        
        {!! BootForm::submit(trans('price.form-button-create'))->addclass('btn btn-primary') !!}

        {!! BootForm::close() !!}

    </div>
@endsection