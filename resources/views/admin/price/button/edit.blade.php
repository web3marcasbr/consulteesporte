<a class="btn btn-small btn-default" href="{{ URL::to('admin/preco/' . $price->id . '/edit') }}">
    <i class="fa fa-pencil" aria-hidden="true"></i>
    @lang('price.button-edit') </a>