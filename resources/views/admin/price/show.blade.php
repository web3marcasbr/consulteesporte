@extends('admin.master')

@section('title', trans('post.title-show'))
@section('subtitle', trans('post.subtitle-show'))
@section('breadcrumb', Breadcrumbs::render('price_show', $price))

@section('content')
    <div class="row">
        <div class="col-md-12">

            @include('components.alert')

            @if ($post->featured != 1)
                <div class="alert alert-info" role="alert">
                    Essa oferta tem um <strong> destaque</strong> maior na página inicial.
                </div>
            @endif

            <div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">
                <h2> {{ $price->name }} </h2>
                <h4> {{ $price->price }} {{ $price->complement }} </h4>
                {!! $price->text !!}
            </div>

        </div>

        <div class="col-md-12">

            <a class="btn btn-small btn-default" href="{{ URL::to('admin/post/') }}"> @lang('post.button-back') </a>

            @include('admin.price.button.edit')

            @include('admin.price.button.delete')
        </div>
    </div>
@endsection
