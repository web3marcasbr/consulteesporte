<a class="btn btn-xs btn-default " href="{{ URL::to('admin/alunos/' . $student->id . '/edit') }}">
    <i class="fa fa-pencil" aria-hidden="true"></i>
    @lang('user.button-edit') </a>