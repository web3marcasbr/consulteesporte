@extends('admin.master')

@section('title', trans('user.title-index'))
@section('subtitle', trans('user.subtitle-index'))
@section('breadcrumb', Breadcrumbs::render('student'))

@section('content')

    @if( !isset($students) )
        <div class="alert alert-info" role="alert">
            <strong>Ops!</strong> Parece que você não tem nenhum aluno cadastrado ainda <a href="testimonial/create" class="alert-link">clique aqui</a> para criar uma nova.
        </div>
    @endif

    <div class="col-md-12">
        <p> @include('admin.students.button.create') </p>
    </div>

    @foreach($students as $student)
        <div class="ro">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="thumbnail">

                    <div class="caption">
                        <p>
                            <h4> {{ $student->name }} </h4>
                            <h5>
                                Comentários <span class="badge"> {{ count($student->comments) }}</span>
                                | Criado em {{ $student->created_at }}
                            </h5>
                        </p>
                        <p> {{ $student->text}} </p>

                        <div class="custom-form">

                            @include('admin.students.button.edit')

                        </div>

                    </div>
                </div>
            </div>
        </div>

    @endforeach

@endsection