@if ($student->approved == 0)
<span class="label label-default">Pendente</span>
@elseif ($student->approved == 1)
<span class="label label-success">Aprovado</span>
@elseif ($student->approved == 2)
<span class="label label-danger">Rejeitado</span>
@endif