@extends('admin.master')

@section('title', trans('user.title-edit'))
@section('subtitle', trans('user.subtitle-edit'))
@section('breadcrumb', Breadcrumbs::render('student_edit', $student))

@section('content')
    <div class="col-lg-12">

        {!! BootForm::open()->action( route('alunos.update', $student->id) )->put() !!}

        {!! BootForm::bind($student) !!}

        {!! BootForm::text('Nome', 'name')->required() !!}
        {!! BootForm::email('Email', 'email')->required() !!}
        {!! BootForm::password('Nova senha', 'password') !!}

        @include('admin.testimonial.button.cancel')
        {!! BootForm::submit(trans('user.form-btn-submit'))->addclass('btn btn-primary') !!}

        {!! BootForm::close() !!}

        <br>
        @include('admin.students.button.delete')

    </div>
@endsection