@extends('admin.master')

@section('title', trans('user.title-show'))
@section('subtitle', trans('user.subtitle-show'))
@section('breadcrumb', Breadcrumbs::render('student', $student))

@section('content')
    <div class="row">
        <div class="col-md-12">

            @include('components.alert')

            <div class="col-xs-12 col-sm-4 col-md-6 col-lg-6">
                <span class="thumbnail">
                    <img src="{{ asset( $student->image_url) }}" alt="...">
                </span>
            </div>

            <div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">
                <h2> {{ $student->title }} </h2>
                <h4> {{ $student->subtitle }}</h4>
                {!! $student->text !!}
            </div>

        </div>

        <div class="col-md-12">

            <a class="btn btn-small btn-default" href="{{ URL::to('admin/post/') }}"> @lang('user.button-back') </a>

            @include('admin.students.button.edit')

            @include('admin.students.button.delete')
        </div>
    </div>
@endsection
