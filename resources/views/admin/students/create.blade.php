@extends('admin.master')

@section('title', trans('user.title-create'))
@section('subtitle', trans('user.subtitle-create'))
@section('breadcrumb', Breadcrumbs::render('student_create'))

@section('content')
    <div class="col-lg-12">

        {!! BootForm::open()->action(route('alunos.store')) !!}

        {!! BootForm::text('Nome', 'name')->required() !!}
        {!! BootForm::email('Email', 'email')->required() !!}
        {!! BootForm::password('Senha', 'password')->required() !!}
        {!! BootForm::password('Confirme a senha', 'password_confirmation')->required() !!}

        <a class="btn btn-small btn-default" href="{{ URL::to('admin/alunos/') }}"> Voltar </a>
        {!! BootForm::submit(trans('user.form-btn-submit'))->addclass('btn btn-primary') !!}

        {!! BootForm::close() !!}

    </div>
@endsection