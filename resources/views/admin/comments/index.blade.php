@extends('admin.master')

@section('title', trans('comment.title-index'))
@section('subtitle', trans('comment.subtitle-index'))
@section('breadcrumb', Breadcrumbs::render('comment'))

@section('content')

    @if( !isset($comments) )
        <div class="alert alert-info" role="alert">
            <strong>Ops!</strong> Parece que você não tem nenhum testimonialagem cadastrada ainda <a href="testimonial/create" class="alert-link">clique aqui</a> para criar uma nova.
        </div>
    @endif

    <div class="col-md-12">
        <p> @include('admin.comments.button.create') </p>
    </div>

    @foreach($comments as $comment)
        <div class="ro">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="thumbnail">

                    <div class="caption">
                        <h4> Depoimento {{ $comment->id }} - @include('admin.comments.components.label') </h4>
                        <h5> Criado por <strong>{{ $comment->name or $comment->user->name }}</strong> as {{ $comment->created_at }}</h5>
                        <p> {{ $comment->text}} </p>

                        <div class="custom-form">

                            {!! BootForm::open()->action( route('comentarios.update', $comment->id) )->put() !!}
                            <input type="hidden" name="approved" value="1">

                            <button type="submit" class="btn btn-large btn-success btn-custom">
                                <i class="fa fa-thumbs-o-up" aria-hidden="true"></i> Aprovar
                            </button>

                            {!! BootForm::close() !!}

                            {!! BootForm::open()->action( route('comentarios.update', $comment->id) )->put() !!}
                            <input type="hidden" name="approved" value="2">

                            <button type="submit" class="btn btn-large btn-danger btn-custom">
                                <i class="fa fa-thumbs-o-down" aria-hidden="true"></i> Rejeitar
                            </button>

                            {!! BootForm::close() !!}

                            @include('admin.comments.button.edit')

                        </div>

                    </div>
                </div>
            </div>
        </div>

    @endforeach

@endsection