@extends('admin.master')

@section('title', trans('comment.title-edit'))
@section('subtitle', trans('comment.subtitle-edit'))
@section('breadcrumb', Breadcrumbs::render('comment_edit', $comment))

@section('content')
    <div class="col-lg-12">

        {!! BootForm::open()->action( route('comentarios.update', $comment->id) )->put() !!}
        {!! BootForm::bind($comment) !!}

        {!! BootForm::textarea(trans('comment.form-edit_text'), 'text')->rows(5)->required() !!}
        {{-- BootForm::text(trans('comment.form-edit_text'), 'name')->required() --}}
        <label>Nome</label>
        <input name='name' type="text" value="{{ $comment->name or $comment->user->name }}" class="form-control" placeholder="{{ trans('comment.form-name-placeholder') }}" required>
        <br>

        @include('admin.comments.button.cancel')
        {!! BootForm::submit(trans('comment.button-edit'))->addClass('btn btn-primary') !!}

        {!! BootForm::close() !!}

        <br>
        @include('admin.comments.button.delete')

    </div>
@endsection