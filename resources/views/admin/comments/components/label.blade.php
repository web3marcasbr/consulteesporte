@if ($comment->approved == 0)
<span class="label label-default">Pendente</span>
@elseif ($comment->approved == 1)
<span class="label label-success">Aprovado</span>
@elseif ($comment->approved == 2)
<span class="label label-danger">Rejeitado</span>
@endif