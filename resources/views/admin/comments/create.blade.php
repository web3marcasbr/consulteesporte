@extends('admin.master')

@section('title', trans('comment.title-create'))
@section('subtitle', trans('comment.subtitle-create'))
@section('breadcrumb', Breadcrumbs::render('comment_create'))

@section('content')
    <div class="col-lg-12">

        {!! BootForm::open()->action(route('comentarios.store'))->multipart() !!}

        {!! BootForm::textarea(trans('comment.form-text'), 'text')->rows(5)->required() !!}
        {!! BootForm::text('Nome', 'name')->placeholder('Insira o nome completo')->helpblock('Esse nome será mostrado como o nome da pessoa que fez o depoimento')->required() !!}
        @include('admin.comments.button.cancel')
        {!! BootForm::submit(trans('comment.form-btn-submit'))->addclass('btn btn-primary') !!}

        {!! BootForm::close() !!}

    </div>
@endsection