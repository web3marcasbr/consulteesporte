@extends('admin.master')

@section('title', trans('comment.title-show'))
@section('subtitle', trans('comment.subtitle-show'))
@section('breadcrumb', Breadcrumbs::render('comment_show', $comment))

@section('content')
    <div class="row">
        <div class="col-md-12">

            @include('components.alert')

            <div class="col-xs-12 col-sm-4 col-md-6 col-lg-6">
                <span class="thumbnail">
                    <img src="{{ asset( $comment->image_url) }}" alt="...">
                </span>
            </div>

            <div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">
                <h2> {{ $comment->title }} </h2>
                <h4> {{ $comment->subtitle }}</h4>
                {!! $comment->text !!}
            </div>

        </div>

        <div class="col-md-12">

            <a class="btn btn-small btn-default" href="{{ URL::to('admin/post/') }}"> @lang('testimonial.button-back') </a>

            @include('admin.comments.button.edit')

            @include('admin.comments.button.delete')
        </div>
    </div>
@endsection
