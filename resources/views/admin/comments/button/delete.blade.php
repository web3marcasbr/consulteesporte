<!-- Button trigger modal -->
<button type="button" class="btn btn-danger btn-small" data-toggle="modal" data-target="#myModal">
    <i class="fa fa-trash-o" aria-hidden="true"></i> @lang('testimonial.modal-delete_button#1')
</button>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"> @lang('testimonial.modal-delete_confirmation_title') </h4>
            </div>
            <div class="modal-body">
                <p> Deseja excluir esse depoimento: {{ $comment->text }}? </p>
                <strong> @lang('testimonial.modal-delete_warning') </strong>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"> @lang('testimonial.button-cancel') </button>

                {{ Form::open(array('url' => 'admin/comentarios/' . $comment->id, 'class' => 'pull-right')) }}
                {{ Form::hidden('_method', 'DELETE') }}
                {{ Form::submit('Confirmar exclusão', array('class' => 'btn btn-danger')) }}
                {{ Form::close() }}

            </div>
        </div>
    </div>
</div>