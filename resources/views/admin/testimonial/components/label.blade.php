@if ($testimonial->approved == 0)
<span class="label label-default">Pendente</span>
@elseif ($testimonial->approved == 1)
<span class="label label-success">Aprovado</span>
@elseif ($testimonial->approved == 2)
<span class="label label-danger">Rejeitado</span>
@endif