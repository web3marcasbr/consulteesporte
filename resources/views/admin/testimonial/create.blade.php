@extends('admin.master')

@section('title', trans('testimonial.title-create'))
@section('subtitle', trans('testimonial.subtitle-create'))
@section('breadcrumb', Breadcrumbs::render('testimonial_create'))

@section('content')
    <div class="col-lg-12">

        {!! BootForm::open()->action(route('depoimentos.store'))->multipart() !!}

        {!! BootForm::textarea(trans('testimonial.form-text'), 'text')->rows(5)->required() !!}
        {!! BootForm::text('Nome', 'name')->placeholder('Insira o nome completo')->helpblock('Esse nome será mostrado como o nome da pessoa que fez o depoimento')->required() !!}
        {!! BootForm::text('Cidade ou empresa', 'city')->required() !!}
        @include('admin.testimonial.button.cancel')
        {!! BootForm::submit(trans('testimonial.form-btn-submit'))->addclass('btn btn-primary') !!}

        {!! BootForm::close() !!}

    </div>
@endsection