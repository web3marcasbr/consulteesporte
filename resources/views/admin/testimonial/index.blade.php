@extends('admin.master')

@section('title', trans('testimonial.title-index'))
@section('subtitle', trans('testimonial.subtitle-index'))
@section('breadcrumb', Breadcrumbs::render('testimonial'))

@section('content')

    @if( !isset($testimonials) )
        <div class="alert alert-info" role="alert">
            <strong>Ops!</strong> Parece que você não tem nenhum testimonialagem cadastrada ainda <a href="testimonial/create" class="alert-link">clique aqui</a> para criar uma nova.
        </div>
    @endif

    <div class="col-md-12">
        <p> @include('admin.testimonial.button.create') </p>
    </div>

    @foreach($testimonials as $testimonial)
        <div class="ro">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="thumbnail">

                    <div class="caption">
                        <h4> Depoimento {{ $testimonial->id }} - @include('admin.testimonial.components.label') </h4>
                        <h5> Criado por <strong>{{ $testimonial->name or $testimonial->user->name }}</strong> as {{ $testimonial->created_at }}</h5>
                        <p> {{ $testimonial->text}} </p>

                        <div class="custom-form">

                            {!! BootForm::open()->action( route('depoimentos.update', $testimonial->id) )->put() !!}
                            <input type="hidden" name="approved" value="1">

                            <button type="submit" class="btn btn-large btn-success btn-custom">
                                <i class="fa fa-thumbs-o-up" aria-hidden="true"></i> Aprovar
                            </button>

                            {!! BootForm::close() !!}

                            {!! BootForm::open()->action( route('depoimentos.update', $testimonial->id) )->put() !!}
                            <input type="hidden" name="approved" value="2">

                            <button type="submit" class="btn btn-large btn-danger btn-custom">
                                <i class="fa fa-thumbs-o-down" aria-hidden="true"></i> Rejeitar
                            </button>

                            {!! BootForm::close() !!}

                            @include('admin.testimonial.button.edit')

                        </div>

                    </div>
                </div>
            </div>
        </div>

    @endforeach

@endsection