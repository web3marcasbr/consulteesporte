@extends('admin.master')

@section('title', trans('testimonial.title-edit'))
@section('subtitle', trans('testimonial.subtitle-edit'))
@section('breadcrumb', Breadcrumbs::render('testimonial_edit', $testimonial))

@section('content')
    <div class="col-lg-12">

        {!! BootForm::open()->action( route('depoimentos.update', $testimonial->id) )->put() !!}
        {!! BootForm::bind($testimonial) !!}

        {!! BootForm::textarea(trans('testimonial.form-edit_text'), 'text')->rows(5)->required() !!}
        {{-- BootForm::text(trans('testimonial.form-edit_text'), 'name')->required() --}}
        <label>Nome</label>
        <input name='name' type="text" value="{{ $testimonial->name or $testimonial->user->name }}" class="form-control" placeholder="{{ trans('testimonial.form-name-placeholder') }}" required>
        {!! BootForm::text('Cidade ou empresa', 'city')->required() !!}
        <br>

        @include('admin.testimonial.button.cancel')
        {!! BootForm::submit(trans('testimonial.button-edit'))->addClass('btn btn-primary') !!}

        {!! BootForm::close() !!}

        <br>
        @include('admin.testimonial.button.delete')

    </div>
@endsection