@extends('admin.master')

@section('title', trans('testimonial.title-show'))
@section('subtitle', trans('testimonial.subtitle-show'))
@section('breadcrumb', Breadcrumbs::render('testimonial_show', $testimonial))

@section('content')
    <div class="row">
        <div class="col-md-12">

            @include('components.alert')

            <div class="col-xs-12 col-sm-4 col-md-6 col-lg-6">
                <span class="thumbnail">
                    <img src="{{ asset( $testimonial->image_url) }}" alt="...">
                </span>
            </div>

            <div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">
                <h2> {{ $testimonial->title }} </h2>
                <h4> {{ $testimonial->subtitle }}</h4>
                {!! $testimonial->text !!}
            </div>

        </div>

        <div class="col-md-12">

            <a class="btn btn-small btn-default" href="{{ URL::to('admin/post/') }}"> @lang('testimonial.button-back') </a>

            @include('admin.testimonial.button.edit')

            @include('admin.testimonial.button.delete')
        </div>
    </div>
@endsection
