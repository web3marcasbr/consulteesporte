<a class="btn btn-small btn-default" href="{{ URL::to('admin/depoimentos/' . $testimonial->id . '/edit') }}">
    <i class="fa fa-pencil" aria-hidden="true"></i>
    @lang('testimonial.button-edit') </a>