<!-- Bootstrap Core CSS -->
<link href=" https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css " rel="stylesheet">

<!-- Custom CSS -->
<link href=" {{ asset('css/sb-admin.css') }} " rel="stylesheet">

<!-- Morris Charts CSS -->
<link href=" {{ asset('css/plugins/morris.css') }} " rel="stylesheet">

<!-- Custom Fonts -->
<link href=" {{ asset('css/font-awesome.min.css') }} " rel="stylesheet" type="text/css">

<link href=" {{ asset('css/custom.css') }} " rel="stylesheet" type="text/css">

