<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="{{ url('admin') }}"> @lang('client.name') </a>
    </div>
    <!-- Top Menu Items -->
    <ul class="nav navbar-right top-nav">

        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> {{ Auth::user()->name }} <b class="caret"></b></a>
            <ul class="dropdown-menu">
                <li>
                    <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="#"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                </li>
            </ul>
        </li>
    </ul>
    <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
    <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul class="nav navbar-nav side-nav">
            <li class="{{ Request::is('admin') ? 'active' : '' }}">
                <a href="{{ action('IndexController@Dashboardindex') }}"><i class="fa fa-fw fa-dashboard"></i> @lang('admin.nav_dashboard') </a>
            </li>
            <li class="{{ Request::is('admin/preco') ? 'active' : '' }}">
                <a href="{{ route("preco.index") }}"><i class="fa fa-fw fa-money"></i> @lang('admin.nav_prices') </a>
            </li>
            <li class="{{ Request::is('admin/post') ? 'active' : '' }}">
                <a href="{{ route("post.index") }}"><i class="fa fa-fw fa-newspaper-o"></i> @lang('admin.nav_posts') </a>
            </li>
            <li class="{{ Request::is('admin/depoimentos') ? 'active' : '' }}">
                <a href="{{ route("depoimentos.index") }}"><i class="fa fa-fw fa-bullhorn"></i> @lang('admin.nav_testimonials') </a>
            </li>
            <li class="{{ Request::is('admin/parceria') ? 'active' : '' }}">
                <a href="{{ route("parceria.index") }}"><i class="fa fa-fw fa-user-plus"></i> @lang('admin.nav_partners') </a>
            </li>
            <li class="{{ Request::is('admin/alunos') ? 'active' : '' }}">
                <a href="{{ route("alunos.index") }}"><i class="fa fa-fw fa-group"></i> @lang('admin.nav_students') </a>
            </li>
            <li>
                <a href="{{ url('admin/downloadNewsletter') }}"><i class="fa fa-fw fa-rss"></i> @lang('admin.nav_news') </a>
            </li>

            <!--
            <li>
                <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-arrows-v"></i> Dropdown <i class="fa fa-fw fa-caret-down"></i></a>
                <ul id="demo" class="collapse">
                    <li>
                        <a href="#">Dropdown Item</a>
                    </li>
                    <li>
                        <a href="#">Dropdown Item</a>
                    </li>
                </ul>
            </li>
            -->
        </ul>
    </div>
    <!-- /.navbar-collapse -->
</nav>