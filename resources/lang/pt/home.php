<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Home language lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the home section
    |
    */

    //General information
    'name' => 'Consulte Esporte',
    'fullname' => '',

    //Navbar
    'navbar_name' => 'Home',

];
