<?php

return [

    /*
    |--------------------------------------------------------------------------
    | testimonial language lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the testimonial section
    |
    */

    'title' => 'Alunos',
    'subtitle' => 'Lorem ipsum dolor sit amet',

    //Navbar
    'navbar_name' => 'Alunos',

    /*
    |--------------------------------------------------------------------------
    | Form language lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the CRUD (admin only)
    |
    */
    'button-show' => 'Ver aluno',

    /*
     * Cancel
     */
    'button-cancel' => 'Cancelar',
    'button-back' => 'Voltar',

    'testimonial-message' => 'Deixe seu aluno',
    'modal-title' => 'O que achou da Consulte Esportes?',
    'form-name' => 'Qual o seu nome?',
    'form-name-placeholder' => 'Escreva seu nome completo. Exemplo: José da Silva',

    /*
     * Delete
     */
    'button-delete' => 'Excluir',
    //Modal
    'modal-delete_button#1' => 'Excluir aluno',
    'modal-delete_confirmation_title' => 'Confirmação de exclusão',
    'modal-delete_confirmation_message' => 'Você deseja mesmo excluir o aluno:',
    'modal-delete_warning' => 'Esta operação é irreversível!',

    /*
     * Edit
     */
    'button-edit' => 'Editar aluno',
    'title-edit' => 'Aluno',
    'subtitle-edit' => 'Editar',
    'form-edit_text' => 'Edite o aluno',

    /*
     * Create
     */
    //general information
    'title-create' => 'Aluno',
    'subtitle-create' => 'Criar novo',
    'button-create' => 'Criar aluno',
    'create-message_success' => 'Aluno criado!',
    'change-message_success' => 'Aluno alterado',

    //Form fields
    'form-title' => 'Titulo',
    'form-title-placeholder' => 'Título para aluno é opcional',
    'form-subtitle' => 'Subtitulo',
    'form-subtitle-placeholder' => 'Uma breve descrição do aluno',
    'form-text' => 'Escreva um aluno',
    'form-text-placeholder' => 'Escreva a postagem aqui',
    'form-private-placeholder' => '',
    'form-btn-submit' => 'Criar aluno',

    /*
     * Index
     */
    'title-index' => 'Alunos',
    'subtitle-index' => 'Cadastrados',

    /*
     * Show
     */
    'title-show' => 'Aluno',
    'subtitle-show' => 'Visualizar',
];
