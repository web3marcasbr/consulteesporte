<?php

return [

    /*
    |--------------------------------------------------------------------------
    | team language lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the team section
    |
    */

    //General page information
    'title' => 'Quem somos',
    'sub_title' => 'Mais de 20 anos de experiência e com parceria de sucesso',

    //Navbar
    'navbar_name' => 'Quem somos',

    //Member #1 biography information
    'member#1_name' => 'Nome completo',
    'member#1_firstname' => 'Primeiro nome',
    'member#1_lastname' => 'Ultimo nome',
    'member#1_function' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam suscipit nisl id nulla vestibulum porta. Cras finibus justo id maximus mollis. Fusce dignissim erat in nisl feugiat posuere. Curabitur sit amet nunc dapibus, lacinia nibh dictum, interdum urna. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas',

    'member#1_biography_title' => 'Curriculo',
    'member#1_biography_description' => 'Lorem ipsum dolor site amet',

    //Member #2 biography information
    'member#2_name' => 'Nome completo',
    'member#2_firstname' => 'Primeiro nome',
    'member#2_lastname' => 'Ultimo nome',
    'member#2_function' => 'Lorem ipsum dolor site amet',

    'member#2_biography_title' => 'Curriculo',
    'member#2_biography_description' => 'Lorem ipsum dolor site amet',

    //Specializations
    'specialization_title' => 'Nossas especializações',
    'specialization_subtitle' => 'Trabalhamos para realizar seus objetivos, sejam eles estéticos,
                                  atléticos ou melhorar sua qualidade de vida.',

    //specialization #1
    'specialization#1_name' => 'Treinamento de Atletas',
    'specialization#1_description' => 'Esportes de Força e Artes Marciais ',

    //specialization #2
    'specialization#2_name' => 'Atividades Físicas',
    'specialization#2_description' => 'para Crianças e Idosos',

    //specialization #3
    'specialization#3_name' => 'Personal Home Care',
    'specialization#3_description' => 'Levamos nossos materiais de treinos
até sua casa ou área aberta!',

    //specialization #4
    'specialization#4_name' => 'Natação',
    'specialization#4_description' => 'para Bebês, Crianças, Adultos e Idosos',

    //specialization #5
    'specialization#5_name' => 'Palestras',
    'specialization#5_description' => 'sobre treinamento com pesos para todos os públicos',

    //specialization #6
    'specialization#6_name' => 'Assessoria Online',
    'specialization#6_description' => 'Montagem dos treinamentos via email + dúvidas via Skype, Whatsapp, Messenger',

    //specialization #7
    'specialization#7_name' => 'Assessoria Esportiva',
    'specialization#7_description' => 'Treinamento semanal online ou presencial',

    //specialization #8
    'specialization#8_name' => 'Personal Trainer',
    'specialization#8_description' => 'Treinamento na academia que você já está matriculado',


];
