<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Client language lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the main active client
    |
    */

    'name' => 'Consulte Esporte',
    'fullname' => '',
    'slogan' => '',

    //Team
    'member#1-name' => 'Osmir da Cunha Filho',
    'member#1-firstname' => 'Osmir da Cunha',
    'member#1-lastname' => 'Filho',
    'member#1-age' => '',
    'member#1-function' => 'Personal trainer',
    'member#1-description' => '
            Formado em Licenciatura Plena em Educação Física pela FEFISA (Faculdades Integradas de Santo André) – 1999.
            Pós-Graduado em Musculação, Treinamento de Força e Condicionamento Físico pela FMU (Faculdades Metropolitanas Unidas) – 2001.
            Atleta de Power Lifting (levantamento de peso) nos anos 90.
            Atleta de luta de braço nos anos 90 e 2000.
            Trabalhou como professor de sala de musculação por 12 anos, sendo que dentro desse tempo atuou também na coordenação da musculação e coordenação geral de academias.
            Personal Trainer desde 1999, nas regiões do ABC Paulista e São Paulo capital, incluindo a Bioritmo Academia e agora na região de Americana e grande Campinas.
            Treinador de atletas de esportes de força (Power lifting, fisiculturismo, luta de braço) desde os anos 90.
            Palestrante sobre temas de terceira idade x envelhecimento x atividade física.
    ',

    'member#2-name' => 'Andréa Barini da Cunha',
    'member#2-firstname' => '',
    'member#2-lastname' => '',
    'member#2-age' => '',
    'member#2-function' => 'Personal Trainer de atividades aquáticas',
    'member#2-description' => 'Formada em licenciatura Plena em Educação Física pela UFAM (Universidade Federal do Amazonas) – 1997.
            Pós-Graduada em Treinamento Desportivo pela FEFISA (Faculdades Integradas de Santo André) – 2001.
            Professora de esportes aquáticos desde 1994, incluindo o Clube dos Sargentos da Amazônia e o Clube Oficial da Polícia Militar de Manaus, assim como academias de médio e grande porte na região do ABC Paulista e agora na região de Americana.
            Trabalha como Personal Trainer de atividades aquáticas para todos os públicos, desde bebês até adultos e atletas.
            ',

    //Contact
    'ddd' => '(19)',
    'cellphone#1' => '99972-9172',
    'cellphone#2' => '',
    'cellphone#3' => '',
    'cellphone#4' => '',
    'phone#1' => '3013-3067',
    'phone#2' => '',
    'phone#3' => '',
    'phone#4' => '',
    'fax' => '',

    'email' => 'contato@consulteesporte.com.br',
    'email#2' => '',
    'email#3' => '',

    //Social Medias
    'instagram-title' => '',
    'instagram-description' => '',
    'instagram-url' => 'https://www.instagram.com/consulteesporte/',

    'facebook-title' => '',
    'facebook-description' => '',
    'facebook-url' => 'http://www.facebook.com.br/consulteesporte/',

    'twitter-title' => '',
    'twitter-description' => '',
    'twitter-url' => '#',

    'linkedin-title' => '',
    'linkedin-description' => '',
    'linkedin-url' => '#',

    'gplus-title' => '',
    'gplus-description' => '',
    'gplus-url' => '#',

    'youtube-title' => '',
    'youtube-description' => '',
    'youtube-url' => '#',

    'spotify-title' => '',
    'spotify-url' => 'https://open.spotify.com/user/consulteesporte',

    //Navbar

];
