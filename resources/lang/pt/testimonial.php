<?php

return [

    /*
    |--------------------------------------------------------------------------
    | testimonial language lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the testimonial section
    |
    */

    'title' => 'Depoimentos',
    'subtitle' => 'Lorem ipsum dolor sit amet',

    //Navbar
    'navbar_name' => 'Depoimentos',

    /*
    |--------------------------------------------------------------------------
    | Form language lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the CRUD (admin only)
    |
    */
    'button-show' => 'Ver depoimento',

    /*
     * Cancel
     */
    'button-cancel' => 'Cancelar',
    'button-back' => 'Voltar',

    'testimonial-message' => 'Clique aqui e deixe seu depoimento!',
    'modal-title' => 'O que achou da Consulte Esportes?',
    'form-name' => 'Qual o seu nome?',
    'form-name-placeholder' => 'Escreva seu nome completo. Exemplo: José da Silva',

    /*
     * Delete
     */
    'button-delete' => 'Excluir',
    //Modal
    'modal-delete_button#1' => 'Excluir depoimento',
    'modal-delete_confirmation_title' => 'Confirmação de exclusão',
    'modal-delete_confirmation_message' => 'Você deseja mesmo excluir o depoimento:',
    'modal-delete_warning' => 'Esta operação é irreversível!',

    /*
     * Edit
     */
    'button-edit' => 'Editar depoimento',
    'title-edit' => 'Depoimento',
    'subtitle-edit' => 'Editar',
    'form-edit_text' => 'Edite o depoimento',

    /*
     * Create
     */
    //general information
    'title-create' => 'Depoimento',
    'subtitle-create' => 'Criar nova',
    'button-create' => 'Criar depoimento',
    'create-message_success' => 'Depoimento criado!',
    'change-message_success' => 'Depoimento alterado',

    //Form fields
    'form-title' => 'Titulo',
    'form-title-placeholder' => 'Título para depoimento é opcional',
    'form-subtitle' => 'Subtitulo',
    'form-subtitle-placeholder' => 'Uma breve descrição do depoimento',
    'form-text' => 'Escreva um depoimento',
    'form-text-placeholder' => 'Escreva a postagem aqui',
    'form-private-placeholder' => '',
    'form-btn-submit' => 'Criar depoimento',

    /*
     * Index
     */
    'title-index' => 'Depoimento',
    'subtitle-index' => 'Publicados',

    /*
     * Show
     */
    'title-show' => 'Depoimento',
    'subtitle-show' => 'Visualizar',
];
