<?php

return [

    /*
    |--------------------------------------------------------------------------
    | pricing language lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the pricing section
    |
    */

    'title' => 'Planos e preços',
    'subtitle' => 'Experiência de mais de 20 anos em diversas áreas da Educação Física',

    //Navbar
    'navbar_name' => 'Planos e preços',

    'modal-delete' => 'Deseja mesmo excluir a oferta:',
    'button-create' => 'Nova oferta',
    /*
    |--------------------------------------------------------------------------
    | Form language lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the CRUD (admin only)
    |
    */
    'button-show' => 'Ver planos',

    /*
     * Cancel
     */
    'button-cancel' => 'Cancelar',
    'button-back' => 'Voltar',


    /*
     * Delete
     */
    'button-delete' => 'Excluir',
    //Modal
    'modal-delete_button#1' => 'Excluir oferta',
    'modal-delete_confirmation_title' => 'Confirmação de exclusão',
    'modal-delete_confirmation_message' => 'Você deseja mesmo excluir a oferta: ',
    'modal-delete_warning' => 'Esta operação é irreversível!',

    /*
     * Edit
     */
    'button-edit' => 'Editar oferta',
    'title-edit' => 'Oferta',
    'subtitle-edit' => 'Editar',

    /*
     * Create
     */
    //general information
    'title-create' => 'Oferta',
    'subtitle-create' => 'Criar nova',
    'form-button-create' => 'Criar oferta',

    //Form fields
    'form-name' => 'Titulo',
    'form-name-placeholder' => 'Nome da oferta',
    'form-price' => 'Preço da oferta',
    'form-price-placeholder' => 'Não coloque R$, apenas números. Exemplo: 53,00',
    'form-complement' => 'Complemento do preço',
    'form-complement-placeholder' => 'Exemplo: /mês, /ano, quinzenal, mensal',
    'form-complement-help' => 'Este texto é opcional e irá aparecer embaixo do valor do preço',
    'form-description' => 'Descreva a oferta',
    'form-description-placeholder' => '',
    'form-featured' => 'Deseja deixar essa oferta destacada?',
    'form-featured-help' => 'A oferta irá ficar com um fundo vermelho na página inicial',
    'form-button-edit' => 'Editar oferta',

    'index-buy' => 'Contratar',

    /*
     * Index
     */
    'title-index' => 'Ofertas',
    'subtitle-index' => 'Publicadas',

    /*
     * Show
     */
    'title-show' => 'Oferta',
    'subtitle-show' => 'Visualizar',

];
