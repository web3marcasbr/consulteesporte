<?php

return [

    /*
    |--------------------------------------------------------------------------
    | footer language lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the footer section
    |
    */

    'social_media-title' => 'Nos Encontre em Nossas Redes Sociais',

];
