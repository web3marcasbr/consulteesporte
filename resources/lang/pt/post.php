<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Blog language lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the blog section
    |
    */

    'title' => 'Blog',
    'subtitle' => 'Acompanhe nossas novidades, sugestões e comentários!',

    //Navbar
    'navbar_name' => 'Blog',

    /*
     * Comments section
     */

    /*
    |--------------------------------------------------------------------------
    | Form language lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the CRUD (admin only)
    |
    */
    'button-show' => 'Ver postagem',

    'exclusive-title' => 'Blog',
    'exclusive-subtitle' => 'Postagens exclusivas para você!',

    /*
     * Cancel
     */
    'button-cancel' => 'Cancelar',
    'button-back' => 'Voltar',


    /*
     * Delete
     */
    'button-delete' => 'Excluir',
    //Modal
    'modal-delete_button#1' => 'Excluir postagem',
    'modal-delete_confirmation_title' => 'Confirmação de exclusão',
    'modal-delete_confirmation_message' => 'Você deseja mesmo excluir a postagem:',
    'modal-delete_warning' => 'Esta operação é irreversível!',

    /*
     * Edit
     */
    'button-edit' => 'Editar postagem',
    'title-edit' => 'Postagem',
    'subtitle-edit' => 'Editar',

    /*
     * Create
     */
    //general information
    'title-create' => 'Postagem',
    'subtitle-create' => 'Criar nova',
    'button-create' => 'Criar postagem',

    //Form fields
    'form-title' => 'Titulo',
    'form-title-placeholder' => 'Coloque um título para a postagem aqui',
    'form-subtitle' => 'Subtitulo',
    'form-subtitle-placeholder' => 'Uma breve descrição da postagem',
    'form-text' => 'Texto da postagem',
    'form-text-placeholder' => 'Escreva a postagem aqui',
    'form-private' => 'Esse contéudo vai ser exclusivo para os alunos?',
    'form-private-placeholder' => '',
    'form-btn-submit' => 'Criar postagem',
    'form-upload' => 'Imagem da postagem',

    /*
     * Index
     */
    'title-index' => 'Postagem',
    'subtitle-index' => 'Publicadas',

    /*
     * Show
     */
    'title-show' => 'Postagem',
    'subtitle-show' => 'Visualizar',






];
