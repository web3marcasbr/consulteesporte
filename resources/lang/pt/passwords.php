<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Senhas tem que ter ao menos 6 caracteres e tem que ser iguais.',
    'reset' => 'Sua senha foi trocada!',
    'sent' => 'Enviamos no seu email um link para redefinir sua senha!',
    'token' => 'Seu token para resetar a senha é inválido.',
    'user' => "Não achamos ninguém cadastrado com esse email. Você digitou certo?",

];
