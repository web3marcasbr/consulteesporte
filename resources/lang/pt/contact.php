<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Contact language lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the contact section
    |
    */

    'title' => 'Contato',
    'subtitle' => 'Entre em contato com a gente!',

    //Form contact #1
    'form_title' => 'Saiba das nossas novidades',
    'form_subtitle' => 'Inscreva-se para receber nossas atualizações',
    'email_field-placeholder' => 'Coloque seu email aqui',
    'button_submit' => 'Inscrever-se',

    //Contact information
    'email' => 'lorem@ipsum.com.br',
    'phone#1' => '(11) 9 97354-9109',
    'phone#2' => '(11) 9 99781-9791',

    //Navbar
    'navbar_name' => 'Contato',

    'email_subs-message' => 'Você foi cadastrado!'

];
