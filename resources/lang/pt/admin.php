<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Home language lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the home section
    |
    */

    //General information
    'name' => 'Consulte Esporte',
    'fullname' => '',

    //Comments
    'comments-main_panel-message' => 'Comentários',
    'comments-main_panel-details' => 'Ver todos',

    //posts
    'posts-main_panel-message' => 'Postagens',
    'posts-main_panel-details' => 'Ver todas',

    //posts
    'testimonial-main_panel-message' => 'Depoimentos',
    'testimonial-main_panel-details' => 'Aprovar/Rejeitar',

    //users
    'students-main_panel-message' => 'Alunos',
    'students-main_panel-details' => 'Ver todos',

    /*
     * Navbar section
     */
    'nav_dashboard' => 'Dashboard',
    'nav_students' => 'Alunos',
    'nav_posts' => 'Postagens',
    'nav_testimonials' => 'Depoimentos',
    'nav_news' => 'Feed',
    'nav_partners' => 'Parceiros',
    'nav_prices' => 'Planos e ofertas',

];
