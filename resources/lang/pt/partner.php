<?php

return [

    /*
    |--------------------------------------------------------------------------
    | partners language lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the partners section
    |
    */

    'title' => 'Parceiros',
    'subtitle' => 'Lorem ipsum dolor sit amet',

    //Navbar
    'navbar_name' => 'Parceiros',

    /*
    |--------------------------------------------------------------------------
    | Form language lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the CRUD (admin only)
    |
    */
    'button-show' => 'Ver parceiro',
    'form-upload' => 'Imagem do parceiro',

    /*
     * Cancel
     */
    'button-cancel' => 'Cancelar',
    'button-back' => 'Voltar',


    /*
     * Delete
     */
    'button-delete' => 'Remover',
    //Modal
    'modal-delete_button#1' => 'Remover parceiro',
    'modal-delete_confirmation_title' => 'Confirmação de remoção',
    'modal-delete_confirmation_message' => 'Você deseja mesmo remover o parceiro:',
    'modal-delete_warning' => 'Esta operação é irreversível!',

    /*
     * Edit
     */
    'button-edit' => 'Editar parceiro',
    'title-edit' => 'Parceiro',
    'subtitle-edit' => 'Editar',
    'form-image_edit' => 'Trocar imagem',
    'form-image_edit-help' => 'Se você escolher uma imagem nova, ela substituirá a atual.',

    /*
     * Create
     */
    //general information
    'title-create' => 'Parceiro',
    'subtitle-create' => 'Adicionar novo',
    'button-create' => 'Adicionar parceiro',

    //Form fields
    'form-name' => 'Nome do parceiro',
    'form-name-placeholder' => '',
    'form-link' => 'Link',
    'form-link-placeholder' => 'URL do parceiro, pode ser site, facebook, instagram etc',
    'form-upload' => 'Logo do parceiro',

    /*
     * Index
     */
    'title-index' => 'Parceiros',
    'subtitle-index' => 'Ativos',

    /*
     * Show
     */
    'title-show' => 'Parceiro',
    'subtitle-show' => 'Visualizar',
];
