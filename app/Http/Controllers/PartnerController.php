<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Partner;

class PartnerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // load the view and pass the partners
        return view('admin.partner.index')->with('partners', Partner::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.partner.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $partner = new Partner;

        $partner->nome = $request->input('nome');
        $partner->link = $request->input('link');
        $partner->image_id = $this->upload($request->image, 'images/partner/');

        $partner->save();

        /*
        return view('admin.partner.show')->with('success', 'Parceiro adicionado!')
            ->with('partner', $partner);
        */

        return redirect('admin/parceria')->with('success', 'Você adicionou um novo parceiro!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('admin.partner.show')->with('partner', Partner::find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.partner.edit')->with('partner', Partner::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $partner = Partner::find($id);

        $image = $request->file('image');

        $partner->nome = $request->input('nome');
        $partner->link = $request->input('link');

        if ( isset($image) )
            $partner->image_id = $this->upload($request->image, 'images/partner/');

        $partner->save();

        return redirect('admin/parceria')->with('success', 'Parceiro foi alterado!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $partner = Partner::find($id);
        $partner->delete();

        return redirect('admin/parceria')->with('success', 'Postagem deletada!');

    }

    function upload($file, $upload_path) {
        $product_path = '/public_html/' . $upload_path;

        $name = explode('.', $file->getClientOriginalName());
        $imageName = $name[0] . '_' . uniqid() . '.' . $file->getClientOriginalExtension();
        $file->move(base_path() . $product_path, $imageName);

        return $upload_path . $imageName;
    }
}
