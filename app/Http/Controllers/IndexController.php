<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Testimonial;
use App\Post;
use App\Partner;
use App\Price;
use App\User;
use App\Comment;
use Auth;

class IndexController extends Controller
{
    public function index()
    {
        $testimonials = Testimonial::where('approved', 1)
            ->inRandomOrder()
            ->get();

        $posts = Post::inRandomOrder()
            ->take(3)
            ->get();

        $prices = Price::take(4)
            ->get();

        $partners = Partner::all();

        return view('master')
            ->with('testimonials', $testimonials)
            ->with('posts', $posts)
            ->with('prices', $prices)
            ->with('partners', $partners);

    }

    public function Dashboardindex()
    {
        $testimonials = Testimonial::where('approved', 0)
            ->get();

        $posts = Post::all();

        $prices = Price::all();

        $partners = Partner::all();

        $comments = Comment::all();

        $users = User::where('admin', 0)
            ->get();

        return view('admin.dashboard')
            ->with('testimonials', $testimonials)
            ->with('posts', $posts)
            ->with('prices', $prices)
            ->with('comments', $comments)
            ->with('users', $users)
            ->with('partners', $partners);

    }

    public function TestimonialStore(Request $request)
    {

        if (isset(Auth::user()->id)) {
            $user = User::find(Auth::user()->id);

            $testimonial = new Testimonial([
                'text' => $request->input('text'),
                'name' => $request->input('name'),
                'city' => $request->input('city'),
                'approved' => 0]);

            $user->testimonials()->save($testimonial);
        } else {

            $testimonial = new Testimonial;

            $testimonial->text = $request->input('text');
            $testimonial->name = $request->input('name');
            $testimonial->city = $request->input('city');
            $testimonial->user_id = 0;
            $testimonial->approved = 0;

            $testimonial->save();
        }

        return redirect()->back()->with('success', 'Depoimento criado');
    }

    public function showBlog($id) {

        $post = Post::find($id);
        $comments = $post->comments;

        return view('page.blog_view')
            ->with('post', Post::find($id))
            ->with('comments', $comments);
    }
}
