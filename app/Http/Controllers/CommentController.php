<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use Auth;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $comments = Comment::all();

        return view('admin.comentarios.index')->with('comments', $comments);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.comentarios.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = User::find(Auth::user()->id);

        if(!empty($user)) {
            $comment = new Testimonial(['text' => $request->input('text'),
                'name' => $request->input('name'),
                'approved' => 0]);

            $user->comentarioss()->save($comment);
        } else {
            $comment = new Testimonial;

            $comment->text = $request->input('text');
            $comment->name = $request->input('name');
            $comment->approved = 0;

            $comment->save();
        }

        /*
        return view('admin.comentarios.show')->with('success', trans('comentarios.create-message_success'))
            ->with('comments', $comment);
        */

        return redirect()->back()->with('success', 'Depoimento criado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('admin.comentarios.show')->with('comments', Comment::find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.comentarios.edit')->with('comments', Comment::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $comment = Comment::find($id);

        $text = $request->input('text');
        $name = $request->input('name');
        $input = $request->input('approved');

        $comment->text = isset($text) ? $text : $comment->text;
        $comment->name = isset($name) ? $name : $comment->name;
        $comment->approved = isset($input) ? $input : $comment->approved;

        $comment->save();

        //return redirect('admin/comentarios')->with('success', 'Depoimento foi alterado!');

        return redirect('admin/comentarios')->with('success', 'Depoimento #' . $comment->id . ' foi alterado!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $comment = Comment::find($id);
        $comment->delete();

        return redirect('admin/comentarios')->with('success', 'Depoimento deletada!');
    }
}
