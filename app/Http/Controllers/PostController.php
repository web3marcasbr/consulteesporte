<?php

namespace App\Http\Controllers;

use App\Image;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Post;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Session;
use App\Comment;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // load the view and pass the posts
        return view('admin.post.index')->with('posts', Post::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.post.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $post = new Post;

        $post->user_id = Auth::user()->id;
        $post->title = $request->input('title');
        $post->subtitle = $request->input('subtitle');
        $post->text = $request->input('text');
        $post->image_url = !empty($request->image) ? $this->upload($request->image, 'images/post/') : '';
        $post->exclusive = $request->input('private');

        $post->save();

        return view('admin.post.show')->with('success', 'Post criado!')
            ->with('post', $post);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id);
        $comments = $post->comments;

        return view('admin.post.show')->with('post', $post)
            ->with('comments', $comments);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.post.edit')->with('post', Post::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = Post::find($id);

        $post->user_id = Auth::user()->id;
        $post->title = $request->input('title');
        $post->subtitle = $request->input('subtitle');
        $post->text = $request->input('text');
        $post->image_url = !empty($request->image) ? $this->upload($request->image, 'images/post/') : $post->image_url;
        $post->exclusive = $request->input('private');

        $post->save();

        return redirect('admin/post')->with('success', 'Postagem foi alterada!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);
        $post->delete();

        return redirect('admin/post')->with('success', 'Postagem deletada!');

    }

    function upload($file, $upload_path) {
        $product_path = '/public_html/' . $upload_path;

        $name = explode('.', $file->getClientOriginalName());
        $imageName = $name[0] . '_' . uniqid() . '.' . $file->getClientOriginalExtension();
        $file->move(base_path() . $product_path, $imageName);

        return $upload_path . $imageName;
    }
}
