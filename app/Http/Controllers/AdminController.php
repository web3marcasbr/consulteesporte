<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\News;
use Excel;

class AdminController extends Controller
{
    public function downloadNewsletter() {
        $news = News::select('email', 'created_at')->get();

        Excel::create('Newsletter - ' . date('d/m/Y'), function ($excel) use ($news) {
            $excel->sheet('Assinantes ' . trans('client.name'), function ($sheet) use ($news) {

                $sheet->cells('A1:C1', function($cells) {
                    $cells->setFontSize(14);
                    $cells->setFontWeight('bold');
                    // manipulate the range of cells
                });

                $sheet->fromArray($news);

            });
        })->download('xls');
    }
}
