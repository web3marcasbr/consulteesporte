<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Email;
use Mail;

class EmailController extends Controller
{
    public function send()
    {

        Mail::send('emails.buy', ['title' => 'teste', 'content' => 'teste'], function ($message)
        {

            $message->from('me@gmail.com', 'Christian Nwamba');

            $message->to('chrisn@scotch.io');

        });

        return response()->json(['message' => 'Request completed']);
    }
}
