<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Session;
use App\Price;

class PriceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $prices = Price::orderby('featured', 'desc')
            ->orderby('created_at', 'desc')
            ->get();

        // load the view and pass the prices
        return view('admin.price.index')->with('prices', $prices);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.price.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $price = new Price;

        $price->name = $request->input('name');
        $price->price = $request->input('price');
        $price->price_complement = $request->input('price_complement');
        $price->description = $request->input('description');
        $price->featured = $request->input('featured');

        $price->save();

        /*
        return view('admin.price.index')->with('success', 'Oferta criada!')
            ->with('price', $price);
        */

        return redirect()->route('preco.index')->with('success', 'Oferta criada');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('admin.price.show')->with('price', Price::find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.price.edit')->with('price', Price::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $price = Price::find($id);

        $price->name = $request->input('name');
        $price->price = $request->input('price');
        $price->price_complement = $request->input('price_complement');
        $price->description = $request->input('description');
        $price->featured = $request->input('featured');

        $price->save();

        return redirect('admin/preco')->with('success', 'Oferta foi alterada!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $price = Price::find($id);

        $price->delete();

        return redirect('admin/preco')->with('success', 'Oferta deletada!');
    }

    function upload($file, $upload_path) {
        $product_path = '/public_html/' . $upload_path;

        $name = explode('.', $file->getClientOriginalName());
        $imageName = $name[0] . '_' . uniqid() . '.' . $file->getClientOriginalExtension();
        $file->move(base_path() . $product_path, $imageName);

        return $upload_path . $imageName;
    }
}
