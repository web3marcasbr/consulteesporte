<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Testimonial;
use App\User;
use Auth;

class TestimonialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $testimonials = Testimonial::orderBy('approved', 'ASC')->get();

        return view('admin.testimonial.index')->with('testimonials', $testimonials);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.testimonial.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = User::find(Auth::user()->id);

        if(!empty($user)) {
            $testimonial = new Testimonial(['text' => $request->input('text'),
                'name' => $request->input('name'),
                'city' => $request->input('city'),
                'approved' => 0]);

            $user->testimonials()->save($testimonial);
        } else {
            $testimonial = new Testimonial;

            $testimonial->text = $request->input('text');
            $testimonial->name = $request->input('name');
            $testimonial->city = $request->input('city');
            $testimonial->approved = 0;

            $testimonial->save();
        }

        /*
        return view('admin.testimonial.show')->with('success', trans('testimonial.create-message_success'))
            ->with('testimonial', $testimonial);
        */

        return redirect()->back()->with('success', 'Depoimento criado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('admin.testimonial.show')->with('testimonial', Testimonial::find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.testimonial.edit')->with('testimonial', Testimonial::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $testimonial = Testimonial::find($id);

        $text = $request->input('text');
        $name = $request->input('name');
        $city = $request->input('city');
        $input = $request->input('approved');

        $testimonial->text = isset($text) ? $text : $testimonial->text;
        $testimonial->name = isset($name) ? $name : $testimonial->name;
        $testimonial->city = isset($city) ? $city : $testimonial->city;
        $testimonial->approved = isset($input) ? $input : $testimonial->approved;

        $testimonial->save();

        //return redirect('admin/testimonial')->with('success', 'Depoimento foi alterado!');

        return redirect('admin/depoimentos')->with('success', 'Depoimento #' . $testimonial->id . ' foi alterado!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $testimonial = Testimonial::find($id);
        $testimonial->delete();

        return redirect('admin/depoimentos')->with('success', 'Depoimento deletada!');
    }
}
