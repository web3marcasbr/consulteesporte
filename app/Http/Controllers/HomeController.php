<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::where('exclusive', 1)
            ->get();

        return view('auth.blog_view')->with('posts', $posts);
    }

    public function blog($id)
    {
        $post = Post::find($id);

        return view('auth.post_view')->with('post', $post)
            ->with('comments', $post->comments);
    }
}
