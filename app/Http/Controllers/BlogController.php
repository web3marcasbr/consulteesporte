<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Http\Requests;

class BlogController extends Controller
{
        public function blogAll()
    {
        $posts = Post::orderBy('created_at', 'DESC')->paginate(4);
        $random_posts = Post::inRandomOrder()->take(6)->get();

        return view('page.blogall')
        	->with('posts', $posts)
        	->with('random_posts', $random_posts);
    }
}
