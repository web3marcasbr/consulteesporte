<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class CheckAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();

        if (isset($user)) {
            if (Auth::user()->admin == 1)
                return $next($request);
            else
                return redirect('login');
        } else {
            return redirect('login');
        }
    }
}
